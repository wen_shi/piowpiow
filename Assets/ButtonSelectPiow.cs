﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class ButtonSelectPiow : Button {

		//Assign sprites for this button
		void Start () {

			normal = PLAYER_ONE_NORMAL;
			down = PLAYER_ONE_DOWN;
			over = PLAYER_ONE_OVER;

			base.Start ();

		}

		protected override void OnMouseDown()
		{
			base.OnMouseDown ();

			//Move to the next loaderthing. 
			PlayerSelectScene.myPlayerType = PlayerSelectScene.PlayerType.piow;
			GameStart.SceneManager.Swap<GameScene>();
		}
	}

}
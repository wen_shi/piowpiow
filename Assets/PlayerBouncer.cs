﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Player bouncer has more stats than the player 
	/// With greater start lives and health. 
	/// </summary>
	public class PlayerBouncer : Player {

		private int myStartLives = 4;
		private int myHealth = 200;
		private int myMaxHealth = 200;

		protected override void SetPlayerStats (int startLives, int health, int maxHealth)
		{
			//set my values to the params
			startLives = myStartLives;
			health = myHealth;
			maxHealth = myMaxHealth;

			//then set it to the player stats. 
			base.SetPlayerStats (startLives, health, maxHealth);
		}

}

}
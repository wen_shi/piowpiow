﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Selecting the player class for Outlaw.
	/// </summary>
	public class ButtonSelectOut : Button {

		//Assign sprites for this button
		void Start () {
			
			normal = PLAYER_TWO_NORMAL;
			down = PLAYER_TWO_DOWN;
			over = PLAYER_TWO_OVER;

			base.Start ();
		
		}

		protected override void OnMouseDown()
		{
			base.OnMouseDown ();

			PlayerSelectScene.myPlayerType = PlayerSelectScene.PlayerType.outlaw;
			GameStart.SceneManager.Swap<GameScene>();
		}
		

	}

}
﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Bullet for outlaw are faster and have 
	/// very low damage. 
	/// </summary>
	public class BulletOutlaw : Bullet {

		private float _myDamage = 20f;
		private float _mySpeed = 100f;

		//Override the deffault damage in start.
		protected override void Start ()
		{
			Damage = _myDamage;
			Speed = _mySpeed;
		}

	}
}
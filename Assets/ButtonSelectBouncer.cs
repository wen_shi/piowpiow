﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class ButtonSelectBouncer : Button {

		//Assign sprites for this button
		void Start () {

			normal = PLAYER_THREE_NORMAL;
			down = PLAYER_THREE_DOWN;
			over = PLAYER_THREE_OVER;

			base.Start ();

		}

		protected override void OnMouseDown()
		{
			base.OnMouseDown ();

			PlayerSelectScene.myPlayerType = PlayerSelectScene.PlayerType.bouncer;
			GameStart.SceneManager.Swap<GameScene>();
		}
	}

}

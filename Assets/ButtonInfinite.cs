﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class ButtonInfinite : Button {

		private ModeSelectScene parentScene;

		//assign sprites
		void Start () {

			print ("start btn select out");

			normal = MODE_INFINITE_NORMAL;
			down = MODE_INFINITE_DOWN;
			over = MODE_INFINITE_OVER;

			base.Start ();

			//Get the mode select scene
			parentScene = transform.parent.gameObject.GetComponent<ModeSelectScene>();
		}

		protected override void OnMouseDown()
		{
			base.OnMouseDown ();

			parentScene.selectMode = TransitionData.Mode.Infinite;
			GameStart.SceneManager.Swap<PlayerSelectScene>();
		}
	}

}

﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

namespace gc{
	
	public class ModeSelectScene :  Scene<TransitionData>{

	//The center of the screen - taken from player select scene
	private Vector3 center = new Vector3 (60f, 40f, 0f);
	private float padding = 25f;

	//Prefab locations 
	private string BTN_CLASSIC= "Prefabs/btn_classic";
	private string BTN_INFINITE = "Prefabs/btn_infinite";

	//Keep track of mode
	//public so buttons can access 
	public TransitionData.Mode selectMode;

	// Use this for initialization
	void Start () {
			
			LoadButtons ();
			selectMode = TransitionData.Mode.Classic;
	}
	
		//from select scene, this should really be a utility script. 
		private void LoadButtons()
		{
			//Make some game objects 
			GameObject buttonObject1, buttonObject2, buttonObject3;
			buttonObject1 = GetButtonObject (BTN_CLASSIC, -padding);
			buttonObject3 = GetButtonObject (BTN_INFINITE, padding);
		}

		//Take a prefab and space it out. 
		private GameObject GetButtonObject(string prefab, float spacing)
		{
			UnityEngine.Object btn = ResourceLoader.Instance.GetResource(prefab);
			GameObject myObject = Instantiate (btn, new Vector3 (center.x + spacing, center.y, center.z), Quaternion.identity) as GameObject;

			myObject.transform.parent = gameObject.transform;
			return myObject;
		}

		internal override TransitionData GetTransitionData ()
		{
			return new TransitionData (){ myMode = selectMode };
		}

}

}
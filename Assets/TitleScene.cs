﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class TitleScene : Scene<TransitionData>  {

		private Vector3 center = new Vector3 (60f, 40f, 0f);
		//A magic center since screen width/height is not right

		internal override void OnEnter(Scene<TransitionData> previousScene)
		{
			//GameStart.ChangeMessage ("Shoot Game");
			//GameStart.Instance().ChangeMessage("Shoot Game");
			//THIS FREEZES UNITY. BUT WHY? 
		}

		// Use this for initialization
		void Start () {
			 
			//Add the start button to the center of the screen
			UnityEngine.Object btn = ResourceLoader.Instance.GetResource("Prefabs/playbutton");
			GameObject button = Instantiate(btn, center, Quaternion.identity) as GameObject;
			button.transform.parent = gameObject.transform; //have it inside the titlescene
		}
		

	}

}
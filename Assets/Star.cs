﻿using UnityEngine;
using System.Collections;

namespace gc{
	
public class Star : MonoBehaviour {

		//attributes for visuals
		private float myLifeTime = 10f; //Make 10 later.
		private float mySignalTime = 5f;
		private float myFasterSignalTime = 8f;
		private Color blinkColor; 
		private float BLINK_REFRESH_TIME = 0.5f;
		private SpriteRenderer sr;
		private bool isFasterSignal = false;

		//audio stuff
		private const string APPEAR_SOUND_LOCATION = "Sounds/powerup_appear";
		private const string DIE_SOUND_LOCATION = "Sounds/powerup_die";
		private const string PLAYER_GET_SOUND_LOCATION = "Sounds/powerup_player";
		private const string ENEMY_GET_SOUND_LOCATION = "Sounds/powerup_enemy";
		private const string SIGNAL_SOUND_LOCATION = "Sounds/powerup_signal";


		//sets variables if necessary before the countdowns
		//Also plays the audio of spawning. 
		void Awake(){

			PlayAudio (APPEAR_SOUND_LOCATION);
			sr = gameObject.GetComponent<SpriteRenderer>();
			blinkColor = new Color(1, 0f, 0f, 0.5f);
		}

		//Gets the audio clip from resources and plays it. 
		private void PlayAudio(string soundLocation)
		{
			AudioClip clip =  (AudioClip)ResourceLoader.Instance.GetResource(soundLocation);
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		}

		/// <summary>
		/// As soon as powerup starts, it starts a counter to
		/// destroying itself (if no one picks it up first).
		/// </summary>
		void Start () {

			//As soon as this starts, prepare it for destruction.
			Invoke("SignalDestroy", mySignalTime);
			Invoke ("FasterSignalDestroy", myFasterSignalTime);

			//Signal that this exists
			EventsManager.Instance().FireEvent(new PowerUpExistsEvent(this));

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		void OnTriggerEnter2D(Collider2D col)
		{
			//If it's a enemy or a player, then they have "taken" the powerup.
			string colTag = col.gameObject.tag;
			string colName = col.gameObject.name;

			if (colTag == "PLAYER" || colName == "PowerShip(Clone)") {
				//First stop all invokes
				CancelInvoke ();
				//Destroy self
				DestroySelf ();

				//not elegant so i am sorry about this 
				//this is bad and i do feel bad
				if (colTag == "PLAYER") {
					ActivatedByPlayer (col.gameObject);
				} else {
					ActivatedByEnemy (col.gameObject);
				}
			}
		}
			
		//find some way to combine them, maybe a utils script?
		#region thissectionisprettyuglyandtakenfromtheplayerblink

		private void SignalDestroy()
		{
			//first making sure the gameobject is not null
			if (gameObject) {
				InvokeRepeating("BlinkIn", 0f, BLINK_REFRESH_TIME * 2f);

			}
		}

		private void FasterSignalDestroy()
		{
			//cancels all
			CancelInvoke ();
			//Start some more blinking (faster)
			isFasterSignal = true;
			InvokeRepeating("BlinkIn", 0f, BLINK_REFRESH_TIME);

			//and get ready to destroy it with the time left
			float timeLeft = myLifeTime - myFasterSignalTime;
			Invoke("DestroySelf", timeLeft);

		}

		/// <summary>
		/// sets the alpha to half of the color, and has a 
		/// different blink rate depending on whether or not
		/// its in the faster stage. 
		/// </summary>
		private void BlinkIn()
		{
			sr.color = blinkColor;
			if (isFasterSignal) 
			{
				PlayAudio (SIGNAL_SOUND_LOCATION);
				Invoke ("BlinkOut", BLINK_REFRESH_TIME * 0.5f);
			} 
			else 
			{
				Invoke ("BlinkOut", BLINK_REFRESH_TIME);
			}
		}

		private void BlinkOut()
		{
			sr.color = Color.white;
		}

		#endregion

		private void DestroySelf()
		{
			//First making sure the gameojbect is not null
			if (gameObject) {

				//Play the gone sound, then destroy the game object. 
				PlayAudio(DIE_SOUND_LOCATION);
				//print ("i am dead goodbye");
				//Fire the dead event
				EventsManager.Instance().FireEvent(new PowerUpDiedEvent(this));
				Destroy (gameObject);
			}
		}

		private void ActivatedByPlayer(GameObject playerObject)
		{
			//print ("activated by player");
			//play sound
			PlayAudio(PLAYER_GET_SOUND_LOCATION);
			//Increase the player's life
			Player playerScript = playerObject.GetComponent<Player>();
			playerScript.AddALife ();

		}

		private void ActivatedByEnemy(GameObject enemyObject)
		{
			//print ("activated by enemy");
			//play sound
			PlayAudio(ENEMY_GET_SOUND_LOCATION);

			//Increase the enemy's life
			Enemy enemyScript = enemyObject.GetComponent<Enemy>();
			enemyScript.Health *= 2;

			//Would start the spawning for another powerup, if applicable. 
		}
			
	}
		

}
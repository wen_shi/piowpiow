﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Big ship movement. Goes really fast until near the player, then slows down
	/// Because it doesn't know what to do it is just a giant thing that cannot shoot.
	/// Being big ship is suffering. 
	/// </summary>
	public class BigShipMovement : EnemyMovement {

		private float distanceToPlayer;
		private float minDistance = 20f;
		private float scaredSpeed = 10f;
		private float boldSpeed = 100f;

		void Start()
		{
			//start out really fast
			MaxSpeed = boldSpeed;
		}

		//Gets the distance to player to figure out how far away it is
		private void CalculateDistanceToPlayer()
		{
			Vector2 playerPosition = playerObject.transform.position;
			Vector2 position = gameObject.transform.position;
			distanceToPlayer = Vector2.Distance (playerPosition, position);
		}

		/// <summary>
		/// Sets the speed for big ship! If the ship is far away (beyond minDistance),
		/// then rush towards the player. Otherwise, go really slow.
		/// </summary>
		protected override void SetSpeed ()
		{
			CalculateDistanceToPlayer ();
			if (distanceToPlayer <= minDistance) {
				MaxSpeed = scaredSpeed;
			} else {
				MaxSpeed = boldSpeed;
			}

			//proceed with regular speed calculations. 
			base.SetSpeed ();
		}
	}

}
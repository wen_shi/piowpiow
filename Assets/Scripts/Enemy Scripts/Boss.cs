﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class Boss : Enemy {

		//Phases for the Boss.
		public enum BossPhase{Appear, SpawnEnemies, Fire, Chase };
		public BossPhase myPhase;

		//Health benchmarks. 
		private float healthMultiplier = 10f;
		private float halfHealth;
		private float lowHealth;

		private TaskManager tm; //Boss is the only thing that needs to
		//Access taskmanager right now, so make it private. 

		private const float ENEMY_SPAWN_DELAY = 2f;
		private const float ENEMY_SPAWN_OFFSET = 0.5f;
		private const int MAX_ENEMY_NUM_IN_BOSS_WAVE = 1;
		//TODO: change this to 10 ^ . for testing. 
		public int enemySpawnedByBossNum;
		//Enemy will access this variable to check if it's a boss
		//Enemy and the last one spawned. 
	
		private const float BULLET_DELAY = 0.1f;

		//Have an instance of this boss.
		private static Boss instance;
		public static Boss Instance(){
			if (instance != null) {
				return instance;
			} else {
				//GameObject bossObject = new GameObject();
				//instance = bossObject.AddComponent<Boss> ();
				//return instance;
				return null; //We actually don't want to return anything
				//If someone asks for Boss before it's spawned properly. 
			}
		}

		//A reference to my movement script
		private BossMovement myMovement;

		void Awake(){
			//start out as appear. 
			myPhase = BossPhase.Appear;
			tm = TaskManager.Instance ();
			instance = this;
			myMovement = gameObject.GetComponent<BossMovement> ();
		}
		void Start () {
			//takes healthmultiplier + 1 hits. The +1 is to maintain sprite size consistency. 
			Health *= (int) healthMultiplier; 
			Health += 100;

			//Set the benchmarks.
			halfHealth = Health * 0.8f; //TODO: set this to 0.5f. 
			lowHealth = Health * 0.2f;

			//Make it bigger. 
			//this should be in APPEAR. 
			Vector3 bigScale = gameObject.transform.localScale * Mathf.RoundToInt(healthMultiplier / 3);
			gameObject.transform.localScale = bigScale;

			//Set the # of enemies spawned by boss
			enemySpawnedByBossNum = 0;

			//call the task stuff
			Debug.Log("Calling task stuff");
			//Task firstTask = new WaitForTask (1f);
			//firstTask.Then(new ActionTask (SpawnEnemy));
			//tm.AddTask (firstTask);
		}

		/// <summary>
		/// Changes the boss phase. Have external class methods
		/// Call this to set a phase instead of constantly checking
		/// for it in update.
		/// </summary>
		/// <param name="newPhase">New phase.</param>
		public void SetBossPhase(BossPhase newPhase)
		{
			//if (myPhase == newPhase) {return;}
			//Debug.Log ("swapped it");
			myPhase = newPhase;

			switch (myPhase) {
			case BossPhase.Appear:
				//Debug.Log ("appearing");
				break;
			case BossPhase.Chase:
				ChasePlayer ();
				break;
			case BossPhase.Fire:
				tm.AbortAllTasks ();
				FireBullets ();
				break;
			case BossPhase.SpawnEnemies:
				SpawnEnemy ();
				Debug.Log ("spawn enemy");
				break;
			default:
				Debug.LogError ("Something went wrong in Boss setting phase" + myPhase);
				break;
			}
		}

		//play a different awake/appear sound
		protected override AudioClip SetAppearAudio (string appearSoundLocation)
		{
			appearSoundLocation = APPEAR_SOUND_BOSS;
			return base.SetAppearAudio (appearSoundLocation);
		}

		//play a different death sound
		protected override void SetDeathAudio (string deathSoundLocation)
		{
			deathSoundLocation = DEATH_SOUND_BOSS;
			base.SetDeathAudio (deathSoundLocation);
		}

		public void SpawnEnemy()
		{

			//Spawn a random enemy, as long as the enemyNum doesn't exceeed
			//The maxinum number.
			if (enemySpawnedByBossNum <= MAX_ENEMY_NUM_IN_BOSS_WAVE) 
			{
				float enemyX = Random.Range (gameObject.transform.position.x - ENEMY_SPAWN_OFFSET,
					              gameObject.transform.position.x + ENEMY_SPAWN_OFFSET);
				float enemyY = Random.Range (gameObject.transform.position.y - ENEMY_SPAWN_OFFSET,
					              gameObject.transform.position.y + ENEMY_SPAWN_OFFSET);

				Vector3 enemyPosition = new Vector3 (enemyX, enemyY, gameObject.transform.position.z);
				Spawner.Spawn ("Prefabs/FastShip", enemyPosition);
				enemySpawnedByBossNum++;
				//Debug.Log (enemySpawnedByBossNum);

				//Start the task for the next enemy. 
				Task newWait = new WaitForTask (ENEMY_SPAWN_DELAY);
				newWait.Then (new ActionTask (SpawnEnemy));
				tm.AddTask (newWait);
			}
		}

		//When getting hit, print out the number for now for debug.
		protected override void ApplyDamage (int damage)
		{
			//Do not apply any damage while it is spawning. 
			if (myPhase != BossPhase.Appear) {
				base.ApplyDamage (damage);
			}

			//After applying damage, check the health to see if 
			//It's at any benchmarks. First check low health.
			//If the boss is at low health, set the state to chase. 
			if (Health <= lowHealth && myPhase == BossPhase.Fire) {
				SetBossPhase (BossPhase.Chase);
			}
			//If boss is at half health, set the state to fire.
			else if (Health <= halfHealth && myPhase == BossPhase.SpawnEnemies) {
				myPhase = BossPhase.Fire;
				SetBossPhase (BossPhase.Fire);
			}
		}

		//For firing bullets
		private void FireBullets()
		{
			myPhase = BossPhase.Fire;

			//EnemyBullet.Fire (Vector2.up, gameObject.transform.position);

				//for debug
				//gameObject.SendMessage ("OnPauseGame", SendMessageOptions.DontRequireReceiver);
			GameObject bulletObject = Spawner.Spawn("Prefabs/EnemyBullet", gameObject.transform.position, 0.1f, 0.3f, 0.1f);
			Bullet bullet = bulletObject.GetComponent<Bullet>();
			Rigidbody2D body = bulletObject.GetComponent<Rigidbody2D>();

			//Fire in a random direction
			float randomX = Random.Range(-1f, 1f);
			float randomY = Random.Range(-1f, 1f);
			Vector2 fireDirection = new Vector2 (randomX, randomY);
			body.AddForce (fireDirection * bullet.Speed, ForceMode2D.Impulse);

			//Play a sound 
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_fire");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, 0.1f);

			//Add this to task. 

			//Start the task for the next enemy. 
			Task newWait = new WaitForTask (BULLET_DELAY);
			newWait.Then (new ActionTask (FireBullets));
			tm.AddTask (newWait);

			//EnemyBullet.FireEnemyBullet (Vector2.up, testPos);
			//So for soem reason fire isn't working so write a workaroudn 

		}
		//For casing the player. 
		private void ChasePlayer()
		{
			//Stop firing bullets
			tm.AbortAllTasks();
			//Change the movement. 
			myMovement.bossSpeed = myMovement.bossChaseSpeed;
			myMovement.SetBossSpeed ();
		}

	}
		

}
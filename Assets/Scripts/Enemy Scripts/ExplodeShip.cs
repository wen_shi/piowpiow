﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace gc{

    //STUFF I NEED TO DO: 
    /// <summary>
    /// ExplodeShip uses the FSM to transition between states
	/// and perform actions dependent on the current ship behavior.
    /// </summary> 
    public class ExplodeShip : Enemy {
		
        #region attributes
        //This prefab has 300 health.
        //It is also slightly smaller
        private float smallMultiplier = 0.75f;

        //Here is the state
        //Have an access to EnemyMovement. 
        protected ExplodeShipMovement movementScript;
		//Keep access to the sprite for pulsing. 
		protected SpriteRenderer sr;
		private int pulseCounter = 0;
		private int maxPulses = 4;

        private FSM<ExplodeShip> states;

		//for comparing health values in pulsing
		private float startingPulseHealth;
		private float finalPulseHealth; 


        #endregion

        /// <summary>
        /// Initialization for the FSM and sets the starting state.
        /// Makes the enemy a little smaller, and reduce the damage. 
        /// </summary>
        void Start()
		{
            //Set EnemyMovement reference and sprite renderer ref 
            movementScript = gameObject.GetComponent<ExplodeShipMovement>();
			sr = gameObject.GetComponent<SpriteRenderer> ();
            //Set FSM init and the starting state
            states = new FSM<ExplodeShip>(this);
            states.TransitionTo<Seeking>();

            //Change the size of the enemy so that it's a little smaller.
			Vector3 tinyScale = gameObject.transform.localScale * smallMultiplier;
			gameObject.transform.localScale = tinyScale;

			//This enemy doesn't hurt the player very much.
			Damage = 50;
		}

        /// <summary>
        /// All this update does
        /// is update the states FSM. 
        /// </summary>
        void Update()
        {

            states.Update();
        }

        /// <summary>
        /// Can be private because no one else needs to access them
        /// ExpllodeShip is the primary state allocator.
        /// </summary>
        #region States
        private class Seeking: FSM<ExplodeShip>.State
        {
            private float minDist = 20f;
			private float startingSpeed = 10f;
            public override void Init()
            {
               // Context.Print("hello"); //Testing
				Context.Print("hello there");
            }

            public override void Update()
            {
				//Move torwards player very slowly.
				//If within range, attack. 
				if (Context.GetPlayerDistance () <= minDist) {
					//Cannot access a nonstatic member of outer type
					//Via nexsted type so... do a workaround. 
					Context.TransitionToPulse();
				}
            }

			public override void OnEnter (FSM<ExplodeShip>.State previousState)
			{
				//Set the speed back to slow
				Context.SetAttackSpeed(startingSpeed);

			}
        }

        private class Pulsing: FSM<ExplodeShip>.State
        {
			public override void Init()
			{
				//Debug.Log ("Entering pulsing stsate");
			}

			public override void OnEnter (FSM<ExplodeShip>.State previousState)
			{
				Context.StopMoving ();
				//Start pulsing. 
				Context.StartPulsing();
			}
        }

        private class Attacking: FSM<ExplodeShip>.State
        {
			private bool isDashing;
			private float fastSpeed = 50f;
			private Vector3 playerPos;
			private int counter;

			public override void Init()
			{
				//Debug.Log ("in attack");
			}

			public override void OnEnter (FSM<ExplodeShip>.State previousState)
			{
				isDashing = false;
				//Get the player position and travel towards it until the 
				//distance traveled is a certain amount.

				Context.SetAttackSpeed (fastSpeed);
				playerPos = Context.GetPlayerPosition();

				//Set the target position
				Context.SetTargetPosition(playerPos);

				isDashing = true;
			}

			public override void Update ()
			{
				if (isDashing) {
					counter++;
					//print (counter);
					//magic numberr!
					if (counter >= 250) {
						Context.TransitionToFleeing ();
					}

					//Move torwads the target (player) pos
					//If a any time the distance rtaveled > max
					//Depending on if it crashed or not have other behioavr. 

				}
			}
        }

        private class Fleeing: FSM<ExplodeShip>.State
        {
			private Vector3 fleePosition;
			private bool goSeek = false;

			public override void Init ()
			{
				print("in fleeing");
			}

			public override void OnEnter (FSM<ExplodeShip>.State previousState)
			{
				//print ("in on enter for fleeing");
				//Get a random world position and go to that
				fleePosition = Game.RandomWorldPosition(3);
				Context.SetTargetPosition (fleePosition);
			}
			public override void Update ()
			{
				base.Update ();
				if (Context.IsSamePosition (fleePosition) && !goSeek) {
					print ("got it");
					//Go to seeking.
					Context.TransitionToSeeking();
					goSeek = true; 
				}

			}
        }
        #endregion

        /// <summary>
        /// Things that can happen.
        /// Refers to move script quite a bit to
        /// get their movestuff. 
        /// C# y u  no support multiple parents
        /// </summary>
        /// <param name="appearSoundLocation"></param>
        /// <returns></returns>
        #region actions
        public void Print(string statement)
        {
           // Debug.Log("Print the statement: " + statement);
        }

        //Get the player distance. 
        public float GetPlayerDistance()
        {
            float distance = movementScript.GetDistanceFromPlayer();
            return distance;
        }

		//Stop moving for Pulsing
		public void StopMoving()
		{
			movementScript.SetSpeed (0);
		}

		//Helper functions!
		public Vector3 GetInitPosition()
		{
			Vector3 initPos = transform.position;
			return initPos;
		}

		public Vector3 GetPlayerPosition()
		{
			return movementScript.GetPlayerPos ();
		}
		public void SetAttackSpeed(float speed)
		{
			//First get the initial position of the object
			movementScript.SetSpeed(speed);
			//Begin moving. 
			//as soon as begin moving, get the distance. 
			//i distance exceeds max distance, stop. 
			//if there's a collision with the player, also stop. 

		}

		public void SetTargetPosition(Vector3 targetPos)
		{
			movementScript.SetTargetPosition (targetPos);
		}

		/// <summary>
		/// Starts the pulsing by making a new GO as a "Ghost"
		/// and having that increase in size. Set the starting pulse
		/// health to compare with ending health to see if it got hit. 
		/// </summary>
		public void StartPulsing()
		{
			//Debug.Log ("Start pulsing");
			//Set the pulse health. 
			startingPulseHealth = Health;

			InvokeRepeating ("MakeAGhost", 0f, 0.5f);
		}

		private void MakeAGhost()
		{
			//Adding things is a lot easier than making a clone then destroying it. 
			GameObject ghost = new GameObject ("Ghost");
			ghost.transform.position = gameObject.transform.position;
			ghost.transform.localRotation = gameObject.transform.localRotation;

			//Make a ghost a child of the object so when it's dead ghost will stop. 
			ghost.transform.parent = gameObject.transform;

			SpriteRenderer ghostSR = ghost.AddComponent<SpriteRenderer> ();
			ghostSR.sprite = sr.sprite;
			ghostSR.sortingLayerName = "ENEMY";

			//Pulse up
			StartCoroutine(WaitAndScaleUp(ghost.transform, ghostSR, 0.01f));

			pulseCounter++;
			if (maxPulses < pulseCounter) {
				CancelInvoke ("MakeAGhost");
				//And here, run what happens afterwards. 

			}
		}

		/// <summary>
		/// Waits the and scale up.
		/// very hacky i am so sorry but i will fifx this later. 
		/// </summary>
		/// <returns>The and scale up.</returns>
		/// <param name="myObject">My object.</param>
		/// <param name="waitTime">Wait time.</param>
		IEnumerator WaitAndScaleUp(Transform myObject, SpriteRenderer sprite, float waitTime)
		{
			//If the scale is greater than 3, stop.
			if (myObject.localScale.x >= 3) {
				//StopCoroutine ("WaitAndScaleUp");
				GetRidOfGhost(myObject);
				yield break;
			}
			float scalePercent = 0.01f;
			myObject.localScale += myObject.localScale * scalePercent;
			float alpha = sprite.color.a - (sprite.color.a * scalePercent);
			sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);
			yield return new WaitForSeconds(waitTime);
			StartCoroutine (WaitAndScaleUp(myObject, sprite, 0.005f));
		}

        // MAKE THEM CHILDREN AND STUFF DO THIS. 
        //DESTROY CHILDREN
		private void GetRidOfGhost(Transform myObject)
		{
			GameObject myObjectGO = myObject.gameObject;
			Destroy (myObjectGO);
			//Get the health of the gameobject.
			finalPulseHealth = Health;
			//Debug.Log ("start health: " + startingPulseHealth
			//+ " end health: " + finalPulseHealth);

			//So if the health was different (was attacked during this phase)
			//Make the phase to flee. Otherwise, make the phase to attack. 

			if (startingPulseHealth == finalPulseHealth) {
				TransitionToAttacking ();
			} else {
				TransitionToFleeing ();
			}
		}

		public bool IsSamePosition(Vector3 pos)
		{
			//have some padding
			float padding = 10f;
			//if (pos == gameObject.transform.position)
			if(pos.x > gameObject.transform.position.x - padding ||
				pos.y > gameObject.transform.position.y - padding ||
				pos.x < gameObject.transform.position.y + padding ||
				pos.y < gameObject.transform.position.y + padding)
			{
				return true;
			} else {
				return false;
			}
		}
		//////////////////////////////////////////////////////////////////
		/// Changing to states, since Unity is being a dumb poop butt.
		/// /////////////////////////////////////////////////////////////
		public void TransitionToSeeking()
		{
			//print ("goin to seek");
			states.TransitionTo<Seeking> ();
		}

		public void TransitionToPulse()
		{
			states.TransitionTo<Pulsing> (); 
		}

		public void TransitionToAttacking()
		{
			states.TransitionTo<Attacking> ();
		}

		public void TransitionToFleeing()
		{
			states.TransitionTo<Fleeing> ();
		}


        #endregion

        #region audio things 
        //play diff. sounds.
        protected override AudioClip SetAppearAudio (string appearSoundLocation)
		{
			appearSoundLocation = APPEAR_SOUND_LOCATION_5;
			return base.SetAppearAudio (appearSoundLocation);
		}

		protected override void SetDeathAudio (string deathSoundLocation)
		{
			deathSoundLocation = DEATH_SOUND_LOCATION_5;
			base.SetDeathAudio (deathSoundLocation);
		}

        #endregion
    }
}

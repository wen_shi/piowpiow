﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Big Ship. Has a lot of health. Starts off big. Shrinks when hit. 
	/// </summary>
	public class BigShip : Enemy {

		private float healthMultiplier = 4f;

		void Start () {
			//takes healthmultiplier + 1 hits. The +1 is to maintain sprite size consistency. 
			Health *= (int) healthMultiplier; 
			Health += 100;

			//Make it bigger. 
			Vector3 bigScale = gameObject.transform.localScale * healthMultiplier;
			gameObject.transform.localScale = bigScale;
		}

		//play a different awake sound 
		protected override AudioClip SetAppearAudio (string appearSoundLocation)
		{
			appearSoundLocation = APPEAR_SOUND_LOCATION_3;
			return base.SetAppearAudio (appearSoundLocation);
		}

		protected override void ApplyDamage (int damage)
		{
			base.ApplyDamage (damage);
			Vector3 smallerScale = gameObject.transform.localScale * (1 - (1 / healthMultiplier));
			gameObject.transform.localScale = smallerScale;
		}
			
		//play a different death sound
		protected override void SetDeathAudio (string deathSoundLocation)
		{
			deathSoundLocation = DEATH_SOUND_LOCATION_3;
			base.SetDeathAudio (deathSoundLocation);
		} 
	
	}

}
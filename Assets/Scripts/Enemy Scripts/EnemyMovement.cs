﻿
using UnityEngine;

namespace gc {

    public class EnemyMovement : MovementController {

        //attributes
		protected float MaxSpeed = 25;
		protected GameObject playerObject;
		protected float currentSpeed;
		protected Player player;

		protected void Start()
		{
			//As soon as you start, set the player. 
			GetPlayer();
			playerObject = player.gameObject;

		}

		protected Player GetPlayer()
		{
			// get the player (if one can't be found bail out)
			player = Player.Instance;
			if (player == null) {
				return null;
			} else
				return player;
		}

        /// <summary>
        /// Ok so fixed update doesn't get called
        /// when time scsale izero so put everything into update. 
        /// </summary>
        protected override void FixedUpdate()
        {

            //if the game is on pause return
            //but why is it not actually happening
            //why i ask myself and maybe whoever else is looking at this
            //why
            if (Game.Instance().IsPaused) { print("an enemy is paused");  return; }

            player = GetPlayer();
            if(player == null) { return; }
            //do this insteaad of get player- the original break one 
            //in fixed update. 
			MoveToPlayer (player);

			SetSpeed ();
			SetDirection ();

        }

		//For enemyManager to access. 
		public virtual void MoveEnemy()
		{
			// get the player (if one can't be found bail out)
			Player player = Player.Instance;
			if (player == null) {
				return;
			}

			MoveToPlayer (player);
			SetSpeed ();
			SetDirection ();
		}
	
		/// <summary>
		/// Moves the player.
		/// </summary>
		/// <param name="player">Player.</param>
		protected virtual void MoveToPlayer(Player player)
		{

			Vector2 directionToPlayer = GetDirectionToPlayer();

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = directionToPlayer * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
			body.AddForce(steeringForce);
		}

		/// <summary>
		/// Gets the direction to player using a gameobject
		/// by calulating the distance+offset from player to eneemy
		/// </summary>
		/// <returns>The direction to player.</returns>
		/// <param name="playerObject">Player object.</param>
		protected virtual Vector2 GetDirectionToPlayer()
		{
			// get the player's position and where it is in relation to this enemy
            if(playerObject == null)
            {
                player = GetPlayer();
                playerObject = player.gameObject;

            }
			Vector2 playerPosition = playerObject.transform.position;
			Vector2 position = gameObject.transform.position;

			Vector2 offsetToPlayer = playerPosition - position;
			Vector2 directionToPlayer = offsetToPlayer.normalized;
			return directionToPlayer;
		}

        /// <summary>
        /// Get distance from player and this ship.
        /// Public so that ExplodeShip FSM can access it. 
        /// </summary>
        /// <returns></returns>
        public virtual float GetDistanceFromPlayer()
        {
			//Set this again because when reading this from the FSM 
			//It's null for some reason. Maybe it doesn't recognize start?
			//Well, this works so it's fine. 
			//Wait no i'm dumb i forgot to do base.Start
			//Duhhhhhhhhhhhhhhhhhhhhhhhhhh. Well keep this in here for safety. 
			if (player == null) {
				player = GetPlayer ();
				playerObject = player.gameObject;
			}

			//Otherwise proceed as usual. 
            Vector2 playerPosition = playerObject.transform.position;
            Vector2 position = gameObject.transform.position;

            float distance = Vector2.Distance(playerPosition, position);
            return distance;
        }

		/// <summary>
		/// Sets the speed.
		/// </summary>
		protected virtual void SetSpeed()
		{
			currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
				body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}
		
		}

		/// <summary>
		/// Sets the direction.
		/// </summary>
		protected virtual void SetDirection()
		{
			// make the body face in the direction of movement
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
		}


    }

}

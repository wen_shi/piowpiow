﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class PowerShipMovement : EnemyMovement {

		private Vector2 targetPos;
		private float changeTargetTime = 3f;
		public Vector2 powerUpPos;
		private Vector2 NULL_VALUE= new Vector2 (-9999, -9999);
		//if you do Vector2? you can let it be null amazing its called a nullable type 

		private float slightlyFasterSpeed = 45f;
		private float slightlySlowerSpeed = 15f;

		protected void Start()
		{
			MaxSpeed = slightlySlowerSpeed;
			InvokeRepeating ("SetNewTarget", 0, changeTargetTime);
			//register for power up existing
			EventsManager.Instance().Register<PowerUpExistsEvent>(OnPowerUpExist);
			EventsManager.Instance().Register<PowerUpDiedEvent>(OnPowerUpDeath);

			powerUpPos = NULL_VALUE;
		}

		/// <summary>
		/// Sets the new target for the ship.
		/// If the power Up Position is a null value (there's no power up)
		/// go to a random spot on the screen. Otherwise, go to the 
		/// power up position. 
		/// </summary>
		public void SetNewTarget()
		{
			//Only change to random if there's no target being chased. 
			if (powerUpPos == NULL_VALUE) {
				targetPos = Game.RandomWorldPosition (3);
				//	print ("changed to" + targetPos);
			} else {
				targetPos = powerUpPos;
			}
		}

		protected override void FixedUpdate()
		{

			//if the game is on pause return
			//but why is it not actually happening
			//why i ask myself and maybe whoever else is looking at this
			//why
			if (Game.Instance().IsPaused) { print("an enemy is paused");  return; }

			player = GetPlayer();
			if(player == null) { return; }
			//do this insteaad of get player- the original break one 
			//in fixed update. 
			MoveToTarget(targetPos);

			SetSpeed ();
			SetDirection ();

		}

		//TODO: change the plaer functions to target in the EnemyMovement script instead 
		//of having this to make the code more elegant. 
		private void MoveToTarget(Vector2 targetPos)
		{
			Vector2 direction = GetDirectionToTarget (targetPos);

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = direction * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
			body.AddForce(steeringForce);
		}

		private Vector2 GetDirectionToTarget(Vector2 target)
		{
			Vector2 position = gameObject.transform.position;

			Vector2 offset = target - position;
			Vector2 direction = offset.normalized;
			return direction;
		}

		//this has to be public so the power ship class can access 
		//it to unregister it when the ship dies. 
		public void OnPowerUpExist(Event e)
		{
			//print ("i exist");
			//If it exists, register for the died event.
			//EventsManager.Instance().Register<PowerUpDiedEvent>(OnPowerUpDeath);
			//Try always registering for power up death

			//if (powerUpPos == NULL_VALUE) {
			//	EventsManager.Instance ().Unregister<PowerUpExistsEvent> (OnPowerUpExist);
			//}

			PowerUpExistsEvent puee = (PowerUpExistsEvent)e;
			//otherwise listen for the event 

			//set the power up position 
			powerUpPos = puee.starPos;
			SetNewTarget ();

		}

		public void OnPowerUpDeath(Event e)
		{
			//print ("power up died");

			//Want to keep the power up exist one in case it spawns again
			//Really only get rid of it when dying. 

			PowerUpDiedEvent pude = (PowerUpDiedEvent)e;

			//tell them to go back to random 
			powerUpPos = NULL_VALUE;
			SetNewTarget ();
		}
			
	}

	//An event to signal that the power up exists
	//So that enemies that are looking for the target can find it. 
	public class PowerUpExistsEvent: Event{

		public readonly Star star;
		public Vector2 starPos;

		public PowerUpExistsEvent(Star star)
		{
			this.star = star;
			this.starPos = star.gameObject.transform.position;

		}

	}

	//An event to signal that the power up has died or stopped existing
	public class PowerUpDiedEvent: Event{

		public readonly Star star;

		public PowerUpDiedEvent(Star star)
		{
			this.star = star;
		}

	}

}
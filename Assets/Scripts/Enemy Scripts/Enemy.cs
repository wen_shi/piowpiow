﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	/// <summary>
	/// Enemy class! Contains information about how much
	/// health the enemy has and how much damage it can take.
	/// </summary>
	public class Enemy : MonoBehaviour {

		//attributes
		public int Health = 100; //made public so movement can access 
		protected int Damage = 100;

		#region audio loading sounds for enemy

		//Appear audio sounds
		protected const string APPEAR_SOUND_LOCATION_1 = "Sounds/enemy_appear_1";
		protected const string APPEAR_SOUND_LOCATION_2 = "Sounds/enemy_appear_2";
		protected const string APPEAR_SOUND_LOCATION_3 = "Sounds/enemy_appear_3";
		protected const string APPEAR_SOUND_LOCATION_4 = "Sounds/enemy_appear_4";
		protected const string APPEAR_SOUND_LOCATION_5 = "Sounds/enemy_appear_5";
		protected const string APPEAR_SOUND_LOCATION_6 = "Sounds/enemy_appear_6";
		protected const string APPEAR_SOUND_BOSS = "Sounds/boss_appear";

		//Death audio sounds
		protected const string DEATH_SOUND_LOCATION_1 = "Sounds/enemy_destroyed_1";
		protected const string DEATH_SOUND_LOCATION_2 = "Sounds/enemy_destroyed_2";
		protected const string DEATH_SOUND_LOCATION_3 = "Sounds/enemy_destroyed_3";
		protected const string DEATH_SOUND_LOCATION_4 = "Sounds/enemy_destroyed_4";
		protected const string DEATH_SOUND_LOCATION_5 = "Sounds/enemy_destroyed_5";
		protected const string DEATH_SOUND_LOCATION_6 = "Sounds/enemy_destroyed_6";
		protected const string DEATH_SOUND_BOSS = "Sounds/boss_destroyed";
		#endregion

		void Awake() {
			EnemyManager.Instance ().NumEnemiesOnScreen++;

			AudioClip clip = SetAppearAudio (APPEAR_SOUND_LOCATION_1);
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);

		}

		/// <summary>
		/// Get the location of the sound appear audio and set it 
		/// </summary>
		/// <param name="appearSoundLocation">Appear sound location.</param>
		protected virtual AudioClip SetAppearAudio(string appearSoundLocation){
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource(appearSoundLocation);
			return clip;
		}
			

		void OnCollisionEnter2D(Collision2D coll) {
			if (coll.gameObject.CompareTag("PLAYER")) {
				Player player = coll.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
				player.SendMessage("ApplyDamage", Damage); 
			}
		}

		protected virtual void ApplyDamage(int damage) {
			Health -= damage;
			//print ("i took this much damage: " + damage);
            CheckForDeath();
		}
        
		protected virtual void CheckForDeath() {
            if (Health <= 0)
            {
				SetDeathAudio (DEATH_SOUND_LOCATION_1);
				//Fire the event just before destruction, which increases the score. 
				EventsManager.Instance().FireEvent(new EnemyDiedEvent(this));
				Destroy(gameObject);

            }
        }
			
		/// <summary>
		/// Get the location of the death audi oand set it 
		/// </summary>
		/// <param name="deathSoundLocation">Death sound location.</param>
		protected virtual void SetDeathAudio(string deathSoundLocation)
		{
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource(deathSoundLocation);
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		}
			
	}



}


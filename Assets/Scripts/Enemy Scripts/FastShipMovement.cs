﻿using UnityEngine;
using System.Collections;

namespace gc{

	public class FastShipMovement : EnemyMovement {

		private float frequency = 5f;
		private float magnitude = 2f;
		//private Vector3 axis;
		private Vector3 pos;

		void Start()
		{
			MaxSpeed = 35;
			pos = transform.position;
			//axis = transform.right; //may need to change to face player. 
		}

		/// <summary>
		/// this is not moving in a sine wave at all but it does a cool
		/// slow  down then speed up thing so eh ill take it 
		/// </summary>
		protected override void SetDirection ()
		{
			Vector2 dir = GetDirectionToPlayer();
			Vector3 directionToPlayer = new Vector3 (dir.x, dir.y, 0);
			pos += directionToPlayer * Time.deltaTime * currentSpeed;
			transform.position = pos + directionToPlayer * Mathf.Sin (Time.time * frequency) * magnitude;
			base.SetDirection ();
		}
	}

}
﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class BossMovement: EnemyMovement {

		public float bossSpeed = 20f; //for debug, set to 50 later TODO 
		public float bossChaseSpeed = 100f;
		// Use this for initialization
		void Start () {
			SetBossSpeed ();
		}

		//For boss to access setting the sped.
		public void SetBossSpeed()
		{
			MaxSpeed = bossSpeed;
		}

	}

}
﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Explode ship movement! By default, follow the player
	/// At a really slow speed. 
	/// </summary>
	public class ExplodeShipMovement : EnemyMovement {

		private float startingSpeed = 10f;
		private bool moveTowardTarget = false;
		private Vector3 targetPosition; //for attacking the player

        void Start()
        {
			base.Start ();
			MaxSpeed = startingSpeed;
        }


		protected override void FixedUpdate()
		{

            //if the game is on pause return
            if (Game.Instance().IsPaused) { return; }

            if (moveTowardTarget) {
				//Debug.Log ("dash" + targetPosition);
				MoveToTarget (targetPosition);
			}
			else
			{
				base.FixedUpdate ();
			}
		}

		public void SetSpeed(float speed)
		{
			MaxSpeed = speed;
		}

		public Vector3 GetPlayerPos()
		{
			return player.transform.position;
		}

		public void SetTargetPosition(Vector3 pos)
		{
			targetPosition = pos;
			//If there's a valid target, chase after that. 
			if (targetPosition != null) {
				moveTowardTarget = true;
			}
		}

		//For moving enemy to a target
		//This replaces all enemy movement.
		public void MoveToTarget(Vector3 target)
		{
			MoveToNotPlayer (target);
			SetSpeed ();
			SetDirection ();
		}

		public void MoveToNotPlayer(Vector3 target)
		{
			Vector2 direction = GetDirectionToTarget();

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = direction * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
			body.AddForce(steeringForce);
		}

		/// <summary>
		/// ///////////////////////////////////////////////////////////
		/// HEY THIS IS NOT REALLY GREAT
		/// BUT IN THE FUTURE
		/// MAKE IT GREATER
		/// //////////////////////////////////////////////////////////////
		/// </summary>
		/// <returns>The direction to target.</returns>
		protected Vector2 GetDirectionToTarget()
		{
			//similar to get to player but with target
			//Later make an overall thing where target is player
			//TODO: this stuff ^
			if (targetPosition != null) {
				Vector3 position = gameObject.transform.position;
				Vector3 offset = targetPosition - position;
				Vector3 direction = offset.normalized;
				direction = new Vector2 (direction.x, direction.y);
				return direction;
			} else {
				print ("uh oh");
				return Vector2.zero;
			}
		}
			
	}
}
﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class FastShip : Enemy {

		//play a different awake/appear sound
		protected override AudioClip SetAppearAudio (string appearSoundLocation)
		{
			appearSoundLocation = APPEAR_SOUND_LOCATION_2;
			return base.SetAppearAudio (appearSoundLocation);
		}

		//play a different death sound
		protected override void SetDeathAudio (string deathSoundLocation)
		{
			deathSoundLocation = DEATH_SOUND_LOCATION_2;
			base.SetDeathAudio (deathSoundLocation);
		}
	}

}
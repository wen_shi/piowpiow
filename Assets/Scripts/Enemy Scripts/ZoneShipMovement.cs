﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class ZoneShipMovement : EnemyMovement {

		private float zoneRadius = 30f;
		private float slightlyFasterSpeed = 45f;
		private ZoneShip zoneShipReference;
		private float radiusIncrease = 5f;
		private float speedIncrease = 5f;


		void Start()
		{
			MaxSpeed = slightlyFasterSpeed;

			zoneShipReference = GetComponent<ZoneShip> ();
			//register for other enemy deaths
			EventsManager.Instance().Register<EnemyDiedEvent>(OnEnemyDeath);
		}

		protected override void FixedUpdate()
		{

            //if the game is on pause return
            if (Game.Instance().IsPaused) { return; }

            // get the player (if one can't be found bail out)
            Player player = Player.Instance;
			if (player == null) {
				return;
			}
			//Debug.Log (zoneRadius);

			//If the player is too far away, just rotate around self.
			if (GetDistanceFromPlayer (player) > zoneRadius) {
				transform.Rotate (Vector3.forward, 5f);

				//If the enemy was already moving from before, 
				//set the forces back to zero.
				body.angularVelocity = 0f;
				body.velocity = Vector3.zero;

			} else {
				//chase the player
				MoveToPlayer(player);
				SetSpeed ();
				SetDirection ();

			}


			//Get distance from player to self.
			//If greater than radius, chase player. 
			//MoveToPlayer (player);
			//SetSpeed ();
			//SetDirection ();

		}

		private float GetDistanceFromPlayer(Player player)
		{
			Vector2 playerPosition = player.transform.position;
			Vector2 position = gameObject.transform.position;

			//calculate dist
			float distance = Vector3.Distance(playerPosition, position);

			return distance;
		}

		/// <summary>
		/// Raises the enemy death event.  (ty unity)
		/// Increases radius and speed 
		/// </summary>
		/// <param name="e">E.</param>
		private void OnEnemyDeath(Event e)
		{
			//If i'm dead, unregister it.
			//It's good to have this here instead of FixedUpdate, because
			//Enemy script will actually kill the GO before it reaches another update,
			//thus unregistered in FixedUpdate will nevre call. 
			if (zoneShipReference.Health <= 0) {
				EventsManager.Instance ().Unregister<EnemyDiedEvent> (OnEnemyDeath);
			}

			//Otherwise, increase the radius and speed. 
			EnemyDiedEvent ede = (EnemyDiedEvent)e;

			//There's gotta be a more elegant way to do this other than the name
			//But if the player kills an enemy of the same type, increase speed+radius by twice as much. 
			//a same enemy died
			if (ede.enemy.gameObject.name == "ZoneShip(Clone)") {
				zoneRadius += (radiusIncrease * 2f);
				MaxSpeed += (speedIncrease * 2f);
			} else { //a different enemy died

				zoneRadius += radiusIncrease;
				MaxSpeed += speedIncrease;
			}
		}

	}

	public class EnemyDiedEvent: Event{
		public readonly Enemy enemy;
		public EnemyDiedEvent(Enemy enemy)
		{

			this.enemy = enemy;
			//Update the score. 
			//	Debug.Log("Game score:" + Game.score);
			Game.UpdateScoreText (Game.score++);

			//If there is a boss on screen, check for that.
			//So you don't go to the next wave. 
			if (Boss.Instance () != null) {
				//So, thisi s always null! 
				//TODO: fix this, somehow. 

			} else {
				EnemyManager.Instance ().NumEnemiesOnScreen--;
				EnemyManager.Instance ().CheckForNextWave ();
			}
		}
	}

}
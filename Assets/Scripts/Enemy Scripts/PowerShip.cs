﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class PowerShip : Enemy {

		//script reference for turning off e vents. 
		private PowerShipMovement psMoveScript;

		void Start()
		{
			//reference to this for script 
			psMoveScript = GetComponent<PowerShipMovement> ();
		}

		//play a different awake/appear sound 
		protected override AudioClip SetAppearAudio (string appearSoundLocation)
		{
			appearSoundLocation = APPEAR_SOUND_LOCATION_6;
			return base.SetAppearAudio (appearSoundLocation);
		}

		//play a different death sound
		protected override void SetDeathAudio (string deathSoundLocation)
		{
			deathSoundLocation = DEATH_SOUND_LOCATION_6;
			base.SetDeathAudio (deathSoundLocation);
		}

		//unregister the power up-related events in here
		//but the main enemy class doesn't care about it
		protected override void CheckForDeath() {
			if (Health <= 0)
			{
				SetDeathAudio (DEATH_SOUND_LOCATION_6);
				//Fire the event just before destruction, which increases the score. 
				EventsManager.Instance().FireEvent(new EnemyDiedEvent(this));

				//unregister the event
				EventsManager.Instance().Unregister<PowerUpDiedEvent>(psMoveScript.OnPowerUpDeath);
				EventsManager.Instance().Unregister<PowerUpExistsEvent>(psMoveScript.OnPowerUpExist);

				//print ("unregistered");
				Destroy(gameObject);

			}
		}
	}

}
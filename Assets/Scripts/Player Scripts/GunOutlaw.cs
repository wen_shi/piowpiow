﻿using UnityEngine;
using System.Collections;

namespace gc{

	public class GunOutlaw : Gun {

		private float spacing = 2f;
		private Vector3 pos1;
		private Vector3 pos2;
		private float myFireRate = 100f;

		protected override void Start ()
		{
			base.Start ();
			//also set the fire interval 
			FireInterval = myFireRate;

			print ("i am start i am outlaw gun");
		}
		protected override void Fire ()
		{
			Vector3 myPos = gameObject.transform.position;
			//Define the two positions as slightly right and left of the object
			pos1 = new Vector3(myPos.x + spacing, myPos.y, myPos.z);
			pos2 = new Vector3 (myPos.x - spacing, myPos.y, myPos.z);

			BulletOutlaw.Fire(_firingDirection, pos1, gameObject.name);
			BulletOutlaw.Fire (_firingDirection, pos2, gameObject.name);
			_nextFireTime = Time.time + (FireInterval / 1000);
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_fire3");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, 0.1f);
		}
}

}
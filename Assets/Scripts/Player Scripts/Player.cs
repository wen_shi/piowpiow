﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

namespace gc {

	/// <summary>
	/// Player, also known as the Piow Standard. 
	/// </summary>
	public class Player : MonoBehaviour {

		//player state
		public enum PlayerState{Spawning, Playing, Dying};
		[HideInInspector]
		public PlayerState myPlayerState;

		//attributes 
		public static Player Instance { get; private set; }
		protected int Health = 100;
		private int MAX_HEALTH = 100;

		//for invincibility frame.
		private float INVINCIBLE_TIME = 2f;
		private SpriteRenderer sr;
		private Color blinkColor;
		private float BLINK_REFRESH_TIME = 0.1f;

		//for hearts
		protected int Lives = 3;
		protected int MAX_LIVES = 6; //max number
        private const float HEART_OFFSET = 1f;
        private Vector3 centerPos;
        private float radius = 3f;
        private ArrayList heartsList;

		void Awake() {

			myPlayerState = PlayerState.Spawning;
			Instance = this;
			//Start invincible feedback
			sr = gameObject.GetComponent<SpriteRenderer>();
			blinkColor = new Color(1, 1, 1, 0.5f);

			//Set lives and health
			//Doesn't do anything in the main piow class. 
			SetPlayerStats(Lives, Health, MAX_HEALTH);
            //spawn hearts object. 
            heartsList = new ArrayList();
            SpawnHearts();
        }
			
		/// <summary>
		/// Sets the player stats.
		/// Lives, Max Health, etc. 
		/// </summary>
		protected virtual void SetPlayerStats(int startLives, int health, int maxHealth)
		{
			Lives = startLives;
			Health = health;
			MAX_HEALTH = maxHealth;
		}

        private void SpawnHearts()
        {
            //Debug.Log("IN spawn hearts function");
            centerPos = gameObject.transform.position;
            //spawn in a circle
          //  Vector3 center = gameObject.transform.position;
            for (int i = 0; i < Lives; i++)
            {
               float increment = (i * 1.0f) / Lives; //just make i a float

                float angle = increment * Mathf.PI * 2; // get the angle for this step (in radians, not degrees)

                // the X &amp; Y position for this angle are calculated using Sin &amp; Cos
                float newX = Mathf.Sin(angle) * radius;
                float newY = Mathf.Cos(angle) * radius;
               Vector3 pos = new Vector3(newX, newY) + centerPos;

                //spawn it
                GameObject temp = Spawner.Spawn("Prefabs/Heart", pos);
                temp.gameObject.transform.parent = gameObject.transform;
                heartsList.Add(temp);
            }

			//After you start register for death because fleetingness of life
			//or some other deep thing
			EventsManager.Instance().Register<PlayerDiedEvent>(OnPlayerDeath);

        }

        private void LoseALife()
        {
            //Debug.Log("removed");
            Lives--;
			Health = MAX_HEALTH; //restore health to full. 
            int removeIndex = heartsList.Count - 1; //consider makign this a STACK. 
            GameObject heart = heartsList[removeIndex] as GameObject;
            heartsList.RemoveAt(removeIndex);
            Destroy(heart);
        }

		//Add a life, currently being used by powerups (star) 
		public void AddALife()
		{
			//As long as it's under the max lives number, do this.
			if (Lives < MAX_LIVES)
			{
				Lives++;
				Health = MAX_HEALTH; //restore heralth

				//Destroy all hearts and re-spawn because it's easier than adding it to the math.
				foreach (GameObject go in heartsList) 
				{
					Destroy (go);
				}

				heartsList.Clear ();
				SpawnHearts ();
			}
		}

        void ApplyDamage(int damage) {
			if (myPlayerState == PlayerState.Playing) {
				Health -= damage;
				CheckForDeath ();
			} else {
				//Debug.Log ("im spawning or dying right now");
			}
		}

        void CheckForDeath()
        {
            if (Health <= 0)
            {
                LoseALife();
				myPlayerState = PlayerState.Dying;
				InvincibleEffect ();
				Invoke ("PlayerFinishedDying", INVINCIBLE_TIME);
                //Event that decreases hearts surrounding palyer. 
                if (Lives <= 0)
                {
					//fire then unregister for event
					EventsManager.Instance().FireEvent(new PlayerDiedEvent(this));
					EventsManager.Instance ().Unregister<PlayerDiedEvent> (OnPlayerDeath);
                    AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_destroyed");
                    AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
                    Destroy(gameObject);
                }
            }
        }

		private void PlayerFinishedDying()
		{
			sr.color = Color.white;
			myPlayerState = PlayerState.Playing;
			CancelInvoke ("BlinkIn");
			CancelInvoke ("BlinkOut");
		}

		private void InvincibleEffect()
		{
			if (myPlayerState != PlayerState.Playing) 
			{
				//assumes basic color is white
				//Blink in, blink out. 
				InvokeRepeating("BlinkIn", 0f, BLINK_REFRESH_TIME * 2);

			}
		}

		private void BlinkIn()
		{
			sr.color = blinkColor;
			Invoke ("BlinkOut", BLINK_REFRESH_TIME);
		}

		private void BlinkOut()
		{
			sr.color = Color.white;
		}

		void OnDestroy() {
			Instance = null;
		
			//Reload here. When player dies, reset the scene so the enemy #'s also reset.
			//This can be done without reloading the scene but this is easier so thanks unity thanks for once 
			//Use the Default unity one since we also declared our own scenemanager. 
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);

		}

		//Sample event handler! 
		private void OnPlayerDeath(Event e)
		{
			PlayerDiedEvent pde = (PlayerDiedEvent)e;
		}
	}

	//Event  class stuff
	public class PlayerDiedEvent:Event
	{
		public readonly Player player;

		public PlayerDiedEvent(Player player)
		{
			this.player = player;
		}
	}

}



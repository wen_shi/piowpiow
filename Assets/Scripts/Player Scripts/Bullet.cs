using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Bullet : MonoBehaviour {

		//Needs to be public so EnemyBullet can access. 
		public float Speed = 75;
		public float Damage = 50; //reduce damage by default so enemies take 2-hits to kill. 

		protected static UnityEngine.Object bulletPrototype;

		//Not necessary, but For cleaner code.
		#region attributesforbulletloading
		private const string _P1NAME = "PlayerShip(Clone)";
		private const string _P2NAME = "PlayerShipBouncer(Clone)";
		private const string _P3NAME = "PlayerShipOutlaw(Clone)";

		private const string _BULLET1_LOCATION = "Prefabs/Bullet";
		private const string _BULLET2_LOCATION = "Prefabs/BulletBouncer";
		private const string _BULLET3_LOCATION = "Prefabs/BulletOutlaw";
		#endregion

		internal static void Fire(Vector2 fireDirection, Vector3 position, string sender)
        {
			if (bulletPrototype == null) {
				SetBulletPrefab (sender);
			}

			GameObject bulletObject = Instantiate(bulletPrototype, position, Quaternion.identity) as GameObject;
			Bullet bullet = bulletObject.GetComponent<Bullet>();
			Rigidbody2D body = bulletObject.GetComponent<Rigidbody2D>();
			body.AddForce(fireDirection * bullet.Speed, ForceMode2D.Impulse);
        }

		/// <summary>
		/// Sets the bullet prefab.
		/// Work around to overriding things with static
		/// variables by using their fire-er/sender names. 
		/// </summary>
		/// <param name="sender">Sender.</param>
		internal static void SetBulletPrefab(string sender)
		{
			//Set the prefab depending on the sender type.
			//Is there a better way to do it?
			switch (sender) {

			case _P1NAME:
				bulletPrototype = ResourceLoader.Instance.GetResource (_BULLET1_LOCATION);
				break;
			case _P2NAME:
				bulletPrototype = ResourceLoader.Instance.GetResource (_BULLET2_LOCATION); 
				break;
			case _P3NAME:
				bulletPrototype = ResourceLoader.Instance.GetResource (_BULLET3_LOCATION);
				break;
			default:
				Debug.LogError ("Could not get the bullet prefab. ABOTR ABORTORT");
				break;
			}

		}

		///Set the speed and damage. Does nothing by deafult. 
		protected virtual void Start(){	}
			

        protected virtual void Update()
		{
            //if the game is on pause return
            if (Game.Instance().IsPaused) { return; }

        }

		protected virtual void OnCollisionEnter2D(Collision2D coll) 
		{
			if (coll.gameObject.tag == "ENEMY") {
				Enemy enemy = coll.gameObject.GetComponent<Enemy>();
				Assert.IsNotNull(enemy, "Object tagged Enemy but no Enemy script attached");
				enemy.SendMessage("ApplyDamage", Damage); 
			}
			Destroy(gameObject);
		}

		/*
		protected virtual void SetBulletType(string prefabLocation)
		{
			bulletPrototype = ResourceLoader.Instance.GetResource(prefabs);
		}*/

	}

}

﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Bouncer moves a lot slower.
	/// </summary>
	public class PlayerMovementBouncer : PlayerMovement {

		private float maxSpeed = 30f;//; //25f;

		protected override void Start ()
		{
			MaxSpeed = maxSpeed;
		}

}

}
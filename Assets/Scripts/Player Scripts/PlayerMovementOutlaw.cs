﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Player movement outlaw moves a lot faster than standard.
	///  
	/// </summary>
	public class PlayerMovementOutlaw : PlayerMovement {
		
		private float maxSpeed = 70f;

		protected override void Start ()
		{
			MaxSpeed = maxSpeed;
		}

}

}
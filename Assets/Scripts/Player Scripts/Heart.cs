﻿using UnityEngine;
using System.Collections;

namespace gc
{

    public class Heart : MonoBehaviour
    {
        private Vector3 target;
        private float angle = 1f;

        void Start()
        {
        }

      void FixedUpdate()
        {
            //TODO: I want to keep the hearts upright! Maybe just move them around a point instead of rotating?
            //Mathy math stuff. 
            target = transform.parent.position;
            //JK i do not want to do that. This only rotates around itself actually. 
            gameObject.transform.RotateAround(target, Vector3.forward, angle);
        }
    }
}

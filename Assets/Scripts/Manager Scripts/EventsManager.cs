﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System; //Need this for System.Type instead of UnityEngine.Types. 

namespace gc{

	//This is just the base class for events. 
	//Use this instead of the UnityEvent class because that 
	//Is just terrible.
	public abstract class Event
	{
		public delegate void Handler(Event e); 
	}

	/// <summary>
	/// Events manager- allows a class to subscribe, unsubscribe, and run
	/// a particular event.  USE THE TERRIBLE UNITY ONE FOR NOW UNTIL YOU
	/// FIGURE OUT HOW TO GET THE TYPES THING WORKING IF POSSIBLE. 
	/// </summary>
	public class EventsManager : MonoBehaviour 
	{

		#region singleton stuff 
		//This should be a singleton, as there is only one instance of it. 
		private static EventsManager instance;

		public static EventsManager Instance()
		{
			//If it exists, return it
			if (instance != null) {
				return instance;
			} else {
				//Otherwise, make one and return that. 
				GameObject EventsManagerObject = new GameObject();
				instance = EventsManagerObject.AddComponent<EventsManager>();

				//This shouldn't ever run because there is one already. If it's null, throw an error. 
				Debug.LogError ("Hey, EventsManager became null. That's not cool.");
				return instance;
			}
		}
		//
		#endregion

		//Dictionary of types. (Specifically, events). 
		//Not unity version
		private Dictionary<Type, Event.Handler> registeredHandlersDict = new Dictionary<Type, Event.Handler>();

		//Unity version. 
		private Dictionary<string, UnityEvent> eventDictionary;

		//------------------------------------------------------------------------------\\
		//- Below is the generic Type version of  handling events using event.handler!--\\
		//------------------------------------------------------------------------------\\

		#region Type version event handling system
		/// <summary>
		/// Register the specified handler. If the handler's type 
		/// is an event (where T:Event), register the action. 
		/// </summary>
		/// <param name="handler">Handler.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void Register<T> (Event.Handler handler)where T:Event
		{
			//Get the type of the generic that's passed in (spoilers its an event)
			//You can't get variables of static types either huh. 
			Type type = typeof(T);
			//If the handlers dictionary has it already, update the handler associated with it
			if (registeredHandlersDict.ContainsKey (type)) {
				registeredHandlersDict [type] += handler;
			} 
			else { //Otherwise, add it as a new dictionary entry/value.
				registeredHandlersDict [type] = handler;
			}
		}

		/// <summary>
		/// Unregister the specified handler. Subtract this 
		/// current handler from the event of registered handlers
		/// if is exists.
		/// </summary>
		/// <param name="handler">Handler.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void Unregister<T> (Event.Handler handler)where T:Event
		{
			Type type = typeof(T);
			Event.Handler handlersOfThisType;
			if(registeredHandlersDict.TryGetValue(type, out handlersOfThisType)){
				handlersOfThisType -= handler;

				//If there's no more handlers, then remove it from the dictionary.
				//Otherwise assign the new value of allHandlers once 
				//this specific handler is removed.
				if (handlersOfThisType == null) {
					registeredHandlersDict.Remove (type);
				} else {
					registeredHandlersDict [type] = handlersOfThisType;
				}
			}
		}

		/// <summary>
		/// Fires/runs the event for every object
		/// registered to the current event of this type
		/// that exists in the registered handlers dictionary.
		/// </summary>
		/// <param name="e">E.</param>
		public void FireEvent(Event e)
		{
			Type type = e.GetType();
			Event.Handler handlersOfThisType;
			if (registeredHandlersDict.TryGetValue (type, out handlersOfThisType)) {
				handlersOfThisType (e); //if it exists run it 
			}
		}

		//Don't really need queued events for anything right now
		//Will add it if it comes up. 

		#endregion

		//------------------------------------------------------------------------------\\
		//----- Below is the unity vesion of handling events using actions -------------\\
		//------------------------------------------------------------------------------\\
		#region unity version 

		/// <summary>
		/// UNITY VERSION of register event. 
		/// If the key (string) of te event exists in the dictionary,
		/// add it. OTherwise, make a new entry and add that. 
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="listener">Listener.</param>
		public static void StartListening(string eventName, UnityAction listener)
		{
			//First start with a new event 
			UnityEvent thisEvent = null;
			//If it exists, get the event associated with it and assign it to thisEvent (out thisEvent)
			//Then add the listener to the event. 
			if (instance.eventDictionary.TryGetValue (eventName, out thisEvent)) {
				thisEvent.AddListener (listener);
			}
			else { 
				//Otherwise if the event dictionay couldn't find it 
				//Make a new entry in the dictionary. 
				thisEvent = new UnityEvent();
				thisEvent.AddListener (listener);
				instance.eventDictionary.Add (eventName, thisEvent);
			}
		}

		/// <summary>
		/// UNITY VERSION of unregister event.
		///  Stops the listening.
		/// If an associated event exists, unsubscribe that particular
		/// action (listener) from the event. 
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="listener">Listener.</param>
		public static void StopListening(string eventName, UnityAction listener)
		{
			//First do a safety check to make sure the instance exists.
			if(instance == null) return;

			//Make a new event and check to see if the action is listening to the event
			UnityEvent thisEvent = null;
			if (instance.eventDictionary.TryGetValue (eventName, out thisEvent)) {
				thisEvent.RemoveListener (listener);
			}
		}

		/// <summary>
		/// UNITY VER of Runs the event.
		/// Runs it for all actions/listeners subscribed/listening 
		/// to the particular event, if the eventName exists in the dict.
		/// </summary>
		/// <param name="eventName">Event name.</param>
		public static void RunEvent(string eventName)
		{
			UnityEvent thisEvent = null;
			//If anything good came out of using the unity actions
			//it's that now im more familiar with the out keyword. 
			if (instance.eventDictionary.TryGetValue (eventName, out thisEvent)) {
				thisEvent.Invoke ();
			}
		}

		#endregion

		//Don't forget to assign the singleton when starting this script. 
		void Awake(){
			instance = this; 
				eventDictionary = new Dictionary<string, UnityEvent>();
		}

	}



}
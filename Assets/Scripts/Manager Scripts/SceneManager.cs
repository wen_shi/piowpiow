﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object; //Wow I did not know you can do this. This is cool! 

namespace gc{

	/// <summary>
	/// Scene manager. Uses TTransitionData, which is the same as the scenes.
	/// This is all mostly to help type safety and everything iscompatiable and 
	/// there are no major explosions of doom. The scenemanager mostly manages
	/// the stack of scenes and forwards events to the current scene. 
	/// </summary>
	public class SceneManager<TTransitionData>{

		internal GameObject SceneRoot{get;set;}

		//Active scenes in the game are organized in a stack 
		//it represents the history of all the relevant scenes
		private readonly Stack<Scene<TTransitionData>> _sceneStack = new Stack<Scene<TTransitionData>>();

		//Doesn't inherit from MonoBehaior, so there's no Start function.
		//Do start stuff inthe constructor if necessary

		//The top most part of the stack is the most recent/current one
		//Sometimes you want to get that so return it
		public Scene<TTransitionData> CurrentScene{
			get
			{
				//if it's not null and there's something in the stack
				//return the top eement (peek) of the stack.
				return _sceneStack.Count != 0 ? _sceneStack.Peek () : null;
			}
		}

		//Push and pop stack elements/scene!

		/// <summary>
		/// Pops the scene. SceneManager needs to let other scenes know what 
		/// scene is replacing and preceding them.  
		/// </summary>
		public void PopScene()
		{
			//Define the previous and next scnes 
			Scene<TTransitionData> previousScene = null;
			Scene<TTransitionData> nextScene = null;

			//Make sure there's something there
			if (_sceneStack.Count != 0) {

				previousScene = _sceneStack.Peek ();
				_sceneStack.Pop ();
			}

			//Do another check in case popping the previous scene destroyed all the elements
			//ie- it only had a stack of counte zero.
			if (_sceneStack.Count != 0) {
				nextScene = _sceneStack.Peek ();
			}

			//if we have a next scene, go though that onenter
			if (nextScene != null) {
				nextScene._OnEnter (previousScene);
			}

			//If there's a prev scene, destroy it and go though that onexit.
			if (previousScene != null) {

				//we dont need this anymore
				Object.Destroy(previousScene.gameObject);
				previousScene._OnExit (nextScene);
			}
		}

		/// <summary>
		/// Pushs the scene.
		/// This methods creates the next scene and pushes
		/// the current scene to the previous.
		/// </summary>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void PushScene<T>() where T: Scene<TTransitionData>
		{

			//define previous and next scenes
			Scene<TTransitionData> previousScene = CurrentScene;
			Scene<TTransitionData> nextScene = CreateScene<T> ();

			_sceneStack.Push (nextScene);
			nextScene._OnEnter (previousScene);

			//make sure previous scene isn't null- it shouldn't be
			//because prevscene is set to our current csene
			//Call the next scene we just created
			//And set the active of the prev scene off. 
			if (previousScene != null) {
				previousScene._OnExit (nextScene);
				previousScene.gameObject.SetActive (false); 
			}

		}

		/// <summary>
		/// Creates the scene. A helper method!
		/// Creates a scene and attaches it to the scenemanager object.
		/// Type safety!!!!
		/// </summary>
		/// <returns>The scene.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		private T CreateScene<T>() where T: Scene<TTransitionData>
		{
			//Make a sceneOBject
			//Make the name whatevre the type of T is
			//In which case should be a Scene 
			GameObject sceneObject = new GameObject { name = typeof(T).Name };
		
			//Set the new scene as child of the root (main) scene
			sceneObject.transform.SetParent (SceneRoot.transform);
			//And return its transitiondata 
			return sceneObject.AddComponent<T>();
		}

		/// <summary>
		/// Swap! When you want to replace the top scene on a stack instead
		/// of creatiing soemthing to push it down. Happens a lot if 
		/// you go back between scenes for example going back to a 
		/// loading screen when the game is done loading 
		/// or maybe even back to the title screen? 
		/// </summary>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void Swap<T>() where T: Scene<TTransitionData>
		{
			//make a previous scene
			Scene<TTransitionData> previousScene = null;

			//If scenes exist in the root, set whatevre the top is to the previous scene. 
			if (_sceneStack.Count > 0) {

				previousScene = _sceneStack.Peek ();
				_sceneStack.Pop ();
			}

			//Make a new scene to replce that with 
			Scene<TTransitionData> nextScene = CreateScene<T> ();
			_sceneStack.Push (nextScene); //Add it to the stack
			nextScene._OnEnter (previousScene); //Do the onenter methods

			//Make sure the prev scene exists (because stak count could be different- 
			//as we added something to it but didn't define the previousscene outside
			//of the conditional of sceneStack having at least 1 scene.
			if (previousScene != null) {

				//We already too kour the previous scene and added a new scene.
				//Dont need this anymore? Destroy it. 
				previousScene._OnExit (nextScene);
				Object.Destroy (previousScene.gameObject);
			}
		}


	}

}
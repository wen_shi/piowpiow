﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace gc{

	/// <summary>
	/// Task manager class! HAndles tasks in a list of tasksList. 
	/// Can add tasks, abort all tasks, handles completion, wait (like a coroutine),
	/// Do an action, and handle task completion. 
	/// </summary>
	public class TaskManager : MonoBehaviour 
	{

		//Singlton to access
		#region singleton
		private static TaskManager instance;
		public static TaskManager Instance(){
			if (instance != null) {
				return instance;
			} else {
				//If it doesn't exist, make one and return that.
				GameObject TaskManageObject = new GameObject();
				instance = TaskManageObject.AddComponent<TaskManager> ();
				return instance;
			}
		}
		#endregion


		//Make a list to hold all the tasks to use!
		private readonly List<Task> tasksList = new List<Task>();

		//An awake that attaches the singleton
		void Awake(){instance = this;}

		//Whether or not the manager has any tasks by 
		//Checking the length of tasksList
		public bool HasTasks()
		{
			return tasksList.Count > 0; //return true if the count > 0, otherwise false. 
		}

		/// <summary>
		/// Adds the task.
		/// 
		///ONLY ADD TAKS THAT AREN'T ALREADY ATTACHED
		///OR ELSE BAD TIME
		/// Will log errors if things go wrong
		/// </summary>
		/// <param name="task">Task.</param>
		public void AddTask(Task task)
		{
			System.Diagnostics.Debug.Assert (task != null); //Send out an error if the task is null
			System.Diagnostics.Debug.Assert(!task.IsAttached()); //Send out an error if task is attached. 
			tasksList.Add(task);
			task.SetStatus (Task.TaskStatus.Pending); //It's attacheed, just waiting. 

		}

		//Go through the list and abort all the tasks.
		public void AbortAllTasks()
		{
			//Go through from the end of the list to the beginning. 
			for (int i = tasksList.Count - 1; i >= 0; --i)
			{

				Task t = tasksList [i];
				t.Abort (); //this sets the status to Abort, then its handled, then detached. 
				tasksList.RemoveAt (i);
				t.SetStatus (Task.TaskStatus.Detached);
			}
		}


		/// <summary>
		/// Update of TaskManager goes through the tasksList,
		/// Puts pending tasks to work(ing), updates all non-completed
		/// tasks, and checks for completion to run HandleCompletion method. 
		/// </summary>
		public void Update () {
		
			//Go through the list from the end to the beginning
			for (int i = tasksList.Count - 1; i >= 0; --i) 
			{

				//Get the current task fro mthe list
				Task currentTask = tasksList [i];

				//If just attached (pending), handle it so its working/running. 
				if (currentTask.IsPending()) 
				{
					currentTask.SetStatus (Task.TaskStatus.Working);
				}

				//If just finished, then handle the completion.
				if (currentTask.IsFinished())
				{
					HandleCompletion (currentTask, i);
				} 
				else 
				{
					//Otherwise if it's not finished, update the task.
					currentTask.Update();
					//And if it finishes after updateing in this frame, hand le the completion.
					if (currentTask.IsFinished())
					{
						HandleCompletion (currentTask, i);
					}
				}
			}
		}

		//Handle completion
		private void HandleCompletion(Task finishedTask, int taskIndex)
		{
			//If there's another task after this one, and this one succeeded,
			//Add the task that is after this one (nextTask). 
			if (finishedTask.NextTask != null && finishedTask.IsSuccessful()) 
			{
				AddTask (finishedTask.NextTask);
			}

			//Remove this task from the list of all tasks
			tasksList.RemoveAt(taskIndex);
			//And set the status to detached.
			finishedTask.SetStatus(Task.TaskStatus.Detached);
		}

		//Abort all tasks! Using a fancy Generic (can only abort things of type Task)
		public void AbortAll<T>() where T:Task 
		{
			Type type = typeof(T);
			//go backwards throuh the list again
			for (int i = tasksList.Count - 1; i >= 0; --i) 
			{
				Task taskToAbort = tasksList [i];
				//Safety check to make sure you're aborting a Task type
				if (taskToAbort.GetType () == type) {
					taskToAbort.Abort ();
				}
			}
		}

		//Wait on a task
		/*
		public WaitTask Wait(double duration)
		{
			WaitTask task = new WaitTask (duration);
			AddTask (task);
			return task;
		}
*/
		//Do an action
		public ActionTask Do(Action action)
		{
			ActionTask task = new ActionTask (action);
			AddTask (task);
			return task;
		}

		//maybe this is never used. 
		//Wait for a task!
		public WaitForTask WaitFor(float waitTime)
		{
			WaitForTask task = new WaitForTask (waitTime);
			AddTask (task);
			return task;
		}
	}

	#region teeny tiny classes that inherit from Task


	public class ActionTask:Task{

		private Action myAction;
		public ActionTask(Action act)
		{

		//	UnityEngine.Debug.Log ("in an actions constructor");
			myAction = act;
		}

		//Call the method. 
		protected override void Init ()
		{
			//UnityEngine.Debug.Log ("hey doing an action");
			base.Init ();
			myAction (); //Calls the action. thanks wyatt 
			SetStatus (TaskStatus.Success); //calling it is a success. 
		}
	}

	public class WaitForTask:Task{

		private float waitTime;
		public WaitForTask(float wt)
		{
			waitTime = wt;
		}

		internal override void Update ()
		{
			base.Update ();
			waitTime -= Time.deltaTime;
			if (waitTime <= 0) {
				SetStatus (TaskStatus.Success);
			}
		}
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Diagnostics;

namespace gc{

	/// <summary>
	/// Task is an abstract class that more detailed tasks (like the boss)
	/// can inherit from and use its methods. Task has a status, and umbrella
	/// methods for what happens when at each of those statuses. 
	/// </summary>
	public abstract class Task {

		//Here are all the possible statuses aka states that a task can have.
		#region Status and accessor
		 
		public enum TaskStatus
		{
			Detached, //Hastn't beeen attached to anything (most likely a task manager) 
			Pending, //Has been attached! But hasn't been intitialized yet
			Working, //has been initialized, is currently working. 
			Success, //succeeded (task complete)
			Fail, //did not succeed  (task complete)
			Aborted //task was interrupted/aborted and did not complete. 
		}

		//readonly variable status, can only be set in this class. 
		private TaskStatus status; 
		public TaskStatus Status{ get { return status; } }  

		#endregion

		//SO IT TURNS OUT Unity doesn't like this setup and keeps giving errors.
		//It says unexpected error => which doesn't make sense since lambda
		//expressions work in C#. 
		#region efficient status checking not working because unity is dumb
		//Here's all the variables for checking status. 
		//so this format is something like:
		//accessor bool varname => (status == taskstatus.state)
		//Its saying if what happens after the => is true
		//then set the variable  to true
		//otherwise set the variable to false. 
		//in this case if my status is taskstatus.state, then varname = true.

		//public bool IsDetached => (status == TaskStatus.Detached);
		//If my status is detached, then isDetached is true.
		//If the status is not detached, then this would be false. 
		//By default, this is true since the constructor makes eveything
		//start at detached.
		//public bool IsAttached => status != TaskStatus.Detached;

		//and here's the rest
		//public bool IsPending => status == TaskStatus.Pending;
		//public bool IsWorking => status == TaskStatus.Working;
		//public bool IsSuccessful => status == TaskStatus.Success;
		///public bool IsFailed => status == TaskStatus.Fail;
		//public bool IsAborted => status == TaskStatus.Aborted;

		//see if its finished, its like an if statmeent. 
		/*
		public bool IsFinished => (
			status == TaskStatus.Fail ||  //if it failed or succeeded or aborted its done
			status == TaskStatus.Success || 
			status == TaskStatus.Aborted);
*/
		#endregion


		#region hideous code warning status checking unity is still dumb
		//whether or not its detached
		public bool IsDetached(){
			if (status == TaskStatus.Detached) {
				return true;
			}
				return false;
		}

		//if its not detached it has to be attached
		public bool IsAttached(){
			if (status != TaskStatus.Detached) {
				return true;
			} 
				return false;
		}

		//if its in these tasks. 
		public bool IsPending() 
		{ 
			if (status == TaskStatus.Pending) {
				return true;
			}
				return false; 
		}

		public bool IsWorking() 
		{ 
			if (status == TaskStatus.Working) {
				return true;
			}
				return false; 
		}
		public bool IsSuccessful() 
		{ 
			if (status == TaskStatus.Success) {
				return true;
			}
				return false; 
		}
		public bool IsFailed() 
		{ 
			if (status == TaskStatus.Fail) {
				return true;
			}
				return false; 
		}
		public bool IsAborted() 
		{ 
			if (status == TaskStatus.Aborted) {
				return true;
			}
				return false; 
		}

		//if its fail, aborted, or success, this means the task is done. 
		public bool IsFinished()
		{
			if (status == TaskStatus.Fail ||
			   status == TaskStatus.Aborted ||
			   status == TaskStatus.Success) {
				return true;
			}
			return false;
		}

		#endregion
		//Constructor 
		protected Task()
		{
			//Whenever a new Task is created, set the status to detached.
			status = TaskStatus.Detached;
		}

		//let other classes abort a task
		public void Abort()
		{
			SetStatus (TaskStatus.Aborted);
		}
			
		/// <summary>
		/// Sets the status.
		/// Don't call too often
		//or bad things will happen.
		/// has access modifier for TYPES and MEMBERS aka i think
		//this will let everything in the namespace to use it. 
		/// </summary>
		/// <param name="newStatus">New status.</param>
		internal void SetStatus(TaskStatus newStatus){

			//if my status is the same as the new status do nothing
			if(status == newStatus) {return; }

			//Otherwise set the status to the new status
			status = newStatus;
			switch (newStatus) 
			{
			case TaskStatus.Working:
				//If something's working then initialize the task
				//Want to seperate from constructor cause some things
				//dont run till after they're made and you dont wanna
				//use constructor values if theyre no longer relevant.
				Init ();
				break;
				//Sucess aborted and fail all run their respective methods
				//then all get cleaned up.
			case TaskStatus.Success:
				OnSuccess ();
				CleanUp ();
				break;
			case TaskStatus.Aborted:
				OnAbort ();
				CleanUp ();
				break;
			case TaskStatus.Fail:
				OnFail ();
				CleanUp();
				break;
			//States mostly relevant for the task manager
			case TaskStatus.Detached:
			case TaskStatus.Pending:
				break;
			default:
				//Soemthing went wrong, throw an error. 
				throw new ArgumentOutOfRangeException (newStatus.ToString(), newStatus, null);
				
			}
		}
			
		/// <summary>
		/// /if a subclass wants to do something different than the default
		//which is nothing so it def. does, then override these
		/// </summary>
		#region methods to be overridden  by subclasses
		//called when the task enters working status
		protected virtual void Init(){}
		//called when a task is done (abort fail or success)
		//called after status change handlers are called. 
		protected virtual void CleanUp(){}
		//What happens when you interrupt a task to abort bfore cleanup
		protected virtual void OnAbort(){}
		//What happens during a success before cleanup
		protected virtual void OnSuccess(){}
		//What happens during a fail before cleanup
		protected virtual void OnFail(){}

		//emergency update in case it is needed
		internal virtual void Update(){}
			
		#endregion

		//Has methods to get and set the next task, and 
		//Then method, which does the task right after the 
		//previous one is done. 
		#region sequencing stuff! super useful
		public Task NextTask{ get; private set; }

		//If a task is abortded or fails it wont run the then part
		//dont assign attached tasks or things will go terribly wrong. 
		public Task Then(Task task)
		{
			//Debug.Assert checks to see if the task is NOT attached.
			//If the condition is false (if the task IS attached, which
			//it should not be, it sends out a Debug message). 
			System.Diagnostics.Debug.Assert(!task.IsAttached());
			NextTask = task;
			return task;
		}

		//concatenate action aka turn them totasks.
		public Task Then(Action action)
		{
			ActionTask task = new ActionTask (action);
			return Then (task);
		}

		#endregion 
	}

}
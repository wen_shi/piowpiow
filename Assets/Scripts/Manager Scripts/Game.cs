﻿using UnityEngine;
using UnityEngine.UI;

namespace gc {

	public class Game : MonoBehaviour {


		//what works inside it
		public enum GameMode { Random, Classic }; //Where Classic means preset levels. 
		public GameMode myGameMode;

		private bool isPaused;
        public bool IsPaused { get { return isPaused; } }
		public GameObject pauseScreen;

		//Keep track of the current player type in this game 
		PlayerSelectScene.PlayerType gamePlayerType;

        //Make this class a singleton to access GameMode, as static enums cannot be changed
        //in the Unity Inspector. 
        #region singleton
        private static Game instance;
        public static Game Instance()
        {
            //If it exists, return it
            if (instance != null)
            {

                return instance;
            }
            else
            {
                //Otherwise, make one and return that. 
                GameObject GameManageObject = new GameObject();
                instance = GameManageObject.AddComponent<Game>();
                return instance;
            }
        }

        #endregion

        #region gui attributes
        //keep the score
        private static Text scoreText;
		[HideInInspector]
		public static int score;

		//keep track of the wave number
		private static Text waveText;
		[HideInInspector]
		public static int wave;

		#endregion

		private const int RANDOM_GUTTER_NUM = 3;

        void Awake() { instance = this; }

		void Start(){

			InitializeGUIElements (); //set them to things
			gamePlayerType = PlayerSelectScene.myPlayerType; //set the player type
			SpawnPlayer (); //spawn that player type 

			//Start the game 
			if (myGameMode == GameMode.Random) 
			{
				EnemyManager.Instance ().CreateRandomEnemies ();
			} 
			else 
			{
				//Debug.LogError ("Hey there! Classic mode isn't ready yet");
				EnemyManager.Instance ().CreateClassicEnemies ();
			}

			if (pauseScreen) {
				pauseScreen.SetActive (false);
			}
		}

		void Update(){

			//Switch pause to whatever the opposite part is.
			if (Input.GetKeyDown (KeyCode.Escape)) {
				isPaused = !isPaused;
				HandlePausing ();
			}

		}
			
		#region gui updates and player spawning
		//Updates for score and wave text
		public static void UpdateScoreText(int numToDisplay)
		{
			scoreText.text = numToDisplay.ToString();
		}

		public static void UpdateWaveText(int numToDisplay)
		{
			waveText.text = "Wave: " + numToDisplay;
		}

        //override to replace with a string
        public static void UpdateWaveText(string textToDisplay)
        {
            waveText.text = textToDisplay;
        }

		//Handl pausing
        //I jus realize i can't call the updates of everything
        //since it does its own things
        //guess ill write those in the fifxedupdates
        //of each subclass if they need it and ovreride update. 
		private void HandlePausing()
		{
			print ("why");
			if (isPaused) {
				pauseScreen.SetActive (true);
				Time.timeScale = 0f;

			} else {
				pauseScreen.SetActive (false);
				Time.timeScale = 1f;
			}
		}

		/// <summary>
		/// Spawns the player depending on 
		/// what the player type is. Returns
		/// an error if type is not set. 
		/// </summary>
		private void SpawnPlayer()
		{
			if (Player.Instance == null) {
				//print ("spawning a player CHANGE WHAT TYPE OF PLAYER SPAWNS HERE");
				switch (gamePlayerType) {

				case PlayerSelectScene.PlayerType.piow:
					Spawner.Spawn("Prefabs/Playership", RandomWorldPosition(RANDOM_GUTTER_NUM));
					break;
				case PlayerSelectScene.PlayerType.bouncer:
					Spawner.Spawn("Prefabs/PlayershipBouncer", RandomWorldPosition(RANDOM_GUTTER_NUM));
					break;
				case PlayerSelectScene.PlayerType.outlaw:
					Spawner.Spawn("Prefabs/PlayershipOutlaw", RandomWorldPosition(RANDOM_GUTTER_NUM));
					break;
				default:
					Debug.LogError ("Player Type not set in SpawnPlayer");
					break;
				}
						
			}
		}

		#endregion

		/// <summary>
		/// Randoms the world position.
		/// Thanks Unity. 
		/// </summary>
		/// <returns>The world position.</returns>
		/// <param name="gutter">Gutter.</param>
		public static Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}

		/// <summary>
		/// Get the center of the screen. Currently for spawning boss. 
		/// </summary>
		/// <returns>The of screen.</returns>
       public static Vector3 CenterOfScreen()
        {
			//Made the Z 1f because it was spawning at -1 for some reason. 
			Vector3 point = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width * 0.5f, Screen.height * 0.5f, 1f));
			return point;

        }
		/// <summary>
		/// Find a score & wave text, and 
		/// do the setup for them. 
		/// </summary>
		private void InitializeGUIElements()
		{	//set everything to default values assign values...etc
			score = 1; //hacky fix to score issue. 
			wave = 1;
			scoreText = GameObject.Find ("Score").GetComponent<Text>();
			waveText = GameObject.Find ("Wave").GetComponent<Text> ();
			waveText.text = "Wave: " + wave;

		}
	}

}


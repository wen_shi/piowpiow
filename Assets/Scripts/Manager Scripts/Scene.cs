﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

namespace gc{

	/// <summary>
	/// Scenes! Parameterized on the type of 
	/// data that's being passed in. All scenes
	/// that belong to a SceneManager have to
	/// be the same type (scene). For safety.
	/// </summary>
	public class Scene<TTransitionData>: MonoBehaviour {

		/// <summary>
		/// Gets the transition data. How the scene gets access to
		/// prev or next scenes info. subclasses that have stuff
		/// to pass on can override this and return o ther stuff. 
		/// </summary>
		/// <returns>The transition data.</returns>
		internal virtual TTransitionData GetTransitionData()
		{
			return default(TTransitionData);
		}

		//On Enter and On Exit methods take care of activitating
		//or deactivating scenes as they transition.
		//There are two versinos of OnEnter/Exit because one of them
		//can be overridden and the other is just waht it does by default
		//so you dont have to keep calling base.onenter/exit. 
		internal void _OnEnter(Scene<TTransitionData> previousScene)
		{
			gameObject.SetActive (true);
			OnEnter (previousScene);
		}

		internal void _OnExit(Scene<TTransitionData> nextScene)
		{
			gameObject.SetActive (false);
			OnExit (nextScene);
		}

		//here's what the subclasses can override
		internal virtual void OnEnter(Scene<TTransitionData> previousScene){}
		internal virtual void OnExit(Scene<TTransitionData> nextScene){ }

		//Don't have to implement destroy/start/init/update cause unity has it
		//yay , unity. 

	}

}
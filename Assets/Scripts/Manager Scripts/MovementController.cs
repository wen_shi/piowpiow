﻿using UnityEngine;

namespace gc {
    /// <summary>
    /// Movement Controller is the parent class of 
    /// anything that (presumably) moves, and has 
    /// a rigidbody attached.- else t hrow error. 
    /// </summary>
    public abstract class MovementController : MonoBehaviour {

		protected Rigidbody2D body;

        void Awake () {
            body = GetComponent<Rigidbody2D>();
            if (body == null) {
                throw new MissingComponentException("MovementController added to game object that has no Rigidbody2D");
            }
        }

        abstract protected void FixedUpdate();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace gc{

	/// <summary>
	/// Handles making waves and what kinds of enemies there are per wave.
	/// Contains a list of all enemies on the screen. 
	/// </summary>
	public class EnemyManager : MonoBehaviour{

		#region fancy singleton stuff
		//Make a fancy singleton.
		private static EnemyManager instance;

		public static EnemyManager Instance()
		{
			//If it exists, return it
			if (instance != null) {

				return instance;
			} else {
				//Otherwise, make one and return that. 
				GameObject EnemyManagerObject = new GameObject();
				instance = EnemyManagerObject.AddComponent<EnemyManager>();
				return instance;
			}
		}

		#endregion

		private const int RANDOM_GUTTER_NUM = 3; //again this will be removed too 
		private const float TIME_BETWEEN_WAVES = 2f;
		#region enemy spawning attributes
		//attributes for enemy
		[HideInInspector]
		public const int STARTING_ENEMY_NUM = 3;
		[HideInInspector]
		public static int minNumEnemies;
		private const int MAX_ENEMY_NUM = 15; //At most have this many enemies on the screen. 
		private int chanceOfSpecialEnemy = 3;
		//numenemies was moved here from enemy class 
		private int numEnemiesOnScreen;
		public int NumEnemiesOnScreen{ get { return numEnemiesOnScreen; } set { numEnemiesOnScreen = value; } }

		//for reinforcements
		private float timeBetweenReinforcements = 3f; //this is an arbitrary number atm. 

		#endregion
		private int maxWave = 12; //arbitrary # to be changed
        private int bossWave = 12; //when boss wave is. Right now for testing. 

		#region enemyList

		private ArrayList enemyList;
		private char[] delim = {','};
		//consts to keep track of enemy prefab
		private const string NORMAL_ENEMY = "Prefabs/EnemyShip";
		private const string FAST_ENEMY = "Prefabs/FastShip";
		private const string BIG_ENEMY = "Prefabs/BigShip";
		private const string ZONE_ENEMY = "Prefabs/ZoneShip";
		private const string BOSS_ENEMY = "Prefabs/BossShip";
		private const string EXPLODE_ENEMY = "Prefabs/ExplodeShip";
		private const string POWER_ENEMY = "Prefabs/PowerShip";

		private GameObject[] currentWaveList;

		//Maybe a better way to do this!?
		private enum EnemyType{ normal, fast, big, zone, explode, power, boss = 9}
		private Dictionary<EnemyType, string> enemyPrefabDict;

		#endregion
		void Awake()
		{
			
            //I FIXED IT. THIS WAS THE PROBLEM. I NEVER ASSIGNED INSTANCE. WOW. WHAT A DUMB MISTAKE. 
            instance = this;

			minNumEnemies = STARTING_ENEMY_NUM;

			//This has to be in awake instead of start. 
			//Map the enemies enums to the dict. 
			enemyPrefabDict = new Dictionary<EnemyType, string>();
			enemyPrefabDict.Add (EnemyType.normal, NORMAL_ENEMY);
			enemyPrefabDict.Add (EnemyType.fast, FAST_ENEMY);
			enemyPrefabDict.Add (EnemyType.big, BIG_ENEMY);
			enemyPrefabDict.Add (EnemyType.zone, ZONE_ENEMY);
			enemyPrefabDict.Add (EnemyType.explode, EXPLODE_ENEMY);
			enemyPrefabDict.Add (EnemyType.power, POWER_ENEMY);
			enemyPrefabDict.Add (EnemyType.boss, BOSS_ENEMY);

			//Make the list, these are all test levels and are bad.
			enemyList = new ArrayList ();
			enemyList.Add ("0, 0, 0"); //0, 0, 0, 0
			enemyList.Add ("0, 0, 0, 0, 1"); //0, 0, 0, 0
			enemyList.Add ("0, 0, 0, 1, 1"); //0, 0, 0, 0
			enemyList.Add ("0, 0, 1, 1, 2"); //0, 0, 0, 0
			enemyList.Add ("0, 1, 1, 2, 2, 3"); //0, 0, 0, 0
			enemyList.Add ("3, 3, 3, 3, 3"); //0, 0, 0, 0
			enemyList.Add ("0, 3, 4, 4"); //0, 3, 3, 1
			enemyList.Add ("0, 5, 5, 1, 2, 2, 4");
			enemyList.Add ("5, 3, 1, 0");
			enemyList.Add ("0, 3, 3, 2");
			enemyList.Add ("5, 1, 1, 2, 0");
			enemyList.Add ("0, 1, 2, 2, 1"); //This is the last wave, you never win but repeat it forever. 

			maxWave = enemyList.Count;
		}
			
		/// <summary>
		/// Creates the enemies, while making sure they don't go above max #.
		/// Update the wave text and start the reinforcements. 
		/// </summary>
		public void CreateRandomEnemies () 
		{
			// Make sure you don't have more than Max num of enemies on the screen. 
			//If you have too many, set the min number equal to the max. 
			if (minNumEnemies > MAX_ENEMY_NUM) 
			{
				minNumEnemies = MAX_ENEMY_NUM;
			}

			//Create this # of enemies

			for (int i = 0; i < minNumEnemies; i++)
			{
				//Enemy type 1: normal ship
				if (Game.RandomWorldPosition (3).x % chanceOfSpecialEnemy == 0) {
					//Have a chance of spawning a different enemy. 
					Spawner.Spawn (NORMAL_ENEMY,  Game.RandomWorldPosition (RANDOM_GUTTER_NUM));
				}
				//Enemy type 2: fast ship
				else if (Game.RandomWorldPosition (3).x % chanceOfSpecialEnemy == 1) {

					Spawner.Spawn (FAST_ENEMY, Game.RandomWorldPosition (RANDOM_GUTTER_NUM));
				}
				//Enemy type 3: big ship 
				else {
					//ok figure out why t his is Enemyship instead of EnemyShip like in resources. 
					Spawner.Spawn (BIG_ENEMY, Game.RandomWorldPosition (RANDOM_GUTTER_NUM));
				}
			}
				
			//update the wave text
			Game.UpdateWaveText (Game.wave++);

			//Spawn reinforcements at a certain rate.
			InvokeRepeating("CreateReinforcements", timeBetweenReinforcements, timeBetweenReinforcements);
		}

		public void CreateClassicEnemies()
		{
			//Get the wave that the game is on and take that string as enemies to spawn. 
			string[] wave;
			int gameWave = Game.wave;

            if (gameWave < maxWave)
            {
                wave = (enemyList[gameWave - 1]).ToString().Split(delim);
            }
            else
			{
				wave =  (enemyList [maxWave - 1]).ToString().Split(delim);
			}

			//Spawn all those. 
			currentWaveList = new GameObject[wave.Length];

            if (gameWave != bossWave)
            {
                for (int j = 0; j < wave.Length; j++)
                {
                    int temp = int.Parse(wave[j]);
                    //Debug.Log ("wave: " + Game.wave + " enemy: " + temp);
                    string enemyPrefab = IntToEnemyString(temp);
                    GameObject enemy = Spawner.Spawn(enemyPrefab, Game.RandomWorldPosition(3));
                    currentWaveList[j] = enemy;
                }
            }
            else
            {
                GameObject boss = Spawner.Spawn(BOSS_ENEMY, Game.CenterOfScreen());
				//Debug.Log ("spawned boss at" + boss.transform.position.x + " , " + boss.transform.position.y);
				//Debug.Log ("center of screen: " + Game.CenterOfScreen ());
                currentWaveList[0] = boss;

            }

			//update the wave text
			Game.UpdateWaveText (Game.wave++);

			//Spawn reinforcements at a certain rate.
			InvokeRepeating("CreateReinforcements", timeBetweenReinforcements, timeBetweenReinforcements);
		}

		/// <summary>
		/// Assigns an int to an enemy and returns the prefab string. 
		/// Only used in the classic mode. 
		/// </summary>
		/// <returns>The to enemy string.</returns>
		/// <param name="value">Value.</param>
		private string IntToEnemyString(int value)
		{
			//Turn it into an enum type from the int. 
			EnemyType myType = (EnemyType)value;

			//Now get the result
			string result;
			enemyPrefabDict.TryGetValue (myType, out result);
			return result;

		}
			 
		/// <summary>
		///	 if all enemies are dead, wait a second then make the next wave.
		/// Also stops the previous wave's reinforcements.
		/// </summary>
		public void CheckForNextWave(){
			if (numEnemiesOnScreen == 0) {
				minNumEnemies++;
				CancelInvoke ("CreateReinforcements");
				//TODO: once the singleton is fixed, go back to using Game's myGameMode. 
				if (Game.Instance().myGameMode == Game.GameMode.Random) {
                    //Debug.Log ("game mode is random"); //TODO: fix the manaegr thing? probably?
                    Game.UpdateWaveText("Get ready");
					Invoke ("CreateRandomEnemies", TIME_BETWEEN_WAVES);
				} else {
                    Game.UpdateWaveText("Get ready");
                    //Debug.LogError ("Hey! I said classic isn't ready yet");
					Invoke ("CreateClassicEnemies", TIME_BETWEEN_WAVES);
				}
			}
		}

		//private cause no one else needs to access this
		//All reinforcements are the default ship because this game is hard enough already. 
		private void CreateReinforcements(){
			//Make sure it's not the boss level
			//if (Game.wave != bossWave) 
			//{
				Spawner.Spawn ("Prefabs/EnemyShip", Game.RandomWorldPosition (RANDOM_GUTTER_NUM));
			//}
		}
			
		#region code thats not being used 
		/// <summary>
		/// Go through the list of enemies in waveList
		/// and update all their movements. 
		/// </summary>
		/// 

		/*
		public void UpdateEnemyMovement()
		{
			for (int i = 0; i < currentWaveList.Length; i++) {
				currentWaveList [i].GetComponent<EnemyMovement>().MoveEnemy ();
			}
		}
		*/

		#endregion
	}

}
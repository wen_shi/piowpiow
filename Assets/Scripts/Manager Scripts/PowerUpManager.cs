﻿using UnityEngine;
using System.Collections;

namespace gc {
	
	public class PowerUpManager : MonoBehaviour {

		private const string STAR_POWER = "Prefabs/Star";
		private const float POWER_TIMER = 11f; //for test
			
		// Use this for initialization
		void Start () {

			InvokeRepeating ("SpawnPower", 0, POWER_TIMER);
		}

		private void SpawnPower()
		{
			Spawner.Spawn (STAR_POWER, Game.RandomWorldPosition (3));
		}
	}

}
﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace gc{

	//finite state machine class! 
	//It's also a generic that's param'd on the context type.
	//No casting, yay!
	public class FSM<TContext> {

		//States access the objects that they represent. 
		//Keep a reference to that so we can access it.
		//Also make it readonly so other things cannot change them.
		//For example the PlayerState access the player obj
		//EnemyState access the enemy obj
		//Etc, it needs to be public or else bad time. 
		public readonly TContext context;

		//Cache it in case need to use them again
		//Saves some time
		private readonly Dictionary<Type, State> stateCache = new Dictionary<Type, State>();
		//know the state.
		public State CurrentState{get; private set;}
	 
		//Constructor
		public FSM(TContext newContext)
		{
            context = newContext;
		}

		//Update method to update the state
		public void Update()
		{
			if (CurrentState != null) {
				CurrentState.Update ();
			}
		}

		//Clear cached states 
		public void Clear()
		{
			foreach(State state in stateCache.Values)
			{
				state.CleanUp();
			}
			stateCache.Clear (); //clear the dict
		}
			
		/// <summary>
		///Transition to a new state
		/// /We have the TState: State for typecast safety
		/// </summary>
		/// <typeparam name="TState">The 1st type parameter.</typeparam>
		public void TransitionTo<TState>() where TState: State
		{
			//Get information about the next state (the one that's passed in)
			//And the previous state (which would be the current one)
			TState nextState = GetOrCreateState<TState> ();
			State previousState = CurrentState;

			//If the previous state exists, then make sure that
			//When it finishes, go to this (next) state
			if (previousState != null) {
                previousState.OnExit(nextState);
			}

			//Set the current to next (whats passed in)
			//And set that prev state (previously current)
			CurrentState = nextState;
			nextState.OnEnter (previousState);
		}

		//Helper method!
		//Manage state instance caching and all that good stuff.
		//If the State is existent in the dictionary, return it. 
		//Otherwise, make a new state.
		private TState GetOrCreateState<TState>() where TState : State
		{
			//Have a state to store information
			State state;
			//If the state has a value associated with it, return
			//its cast as a TState 
			if (stateCache.TryGetValue (typeof(TState), out state)) {
				return (TState)state;
			} else {

				//Nothing found in the dictionary, make a new one.
				//Activator?!? Is required to create things.
				//It contains methos to create objects locally
				TState newState = Activator.CreateInstance<TState>();
				newState.fsm = this; //new states fsm is what created it
				newState.Init (); //run the start function
				stateCache[typeof(TState)] = newState; //Add it to the dictionary
				return newState; //And return it!
			}
		}

		//Make the class State inside here.
		//Since it doesn't need t be referred to anywhere else.
		public abstract class State
		{
			//keep track of what state it belongs to
			internal FSM<TContext> fsm { get; set; }

			//keep track of current state
			public State currentState;
			//Make sure subclasses always have a fsmstate
			//=> is still an unexplected symbol. 
			public TContext Context { get { return fsm.context; } }
			//public TContext Context => fsm.context;

			//make a state thing. 

			//basically init also this happens because
			//unity doesn't like it when i set it above. 
			public virtual void Init(){
				//Context = fsm.context;
			}
				
			//Get the previous state and is called whenver the
			//State becomes active.
			public virtual void OnEnter(State previousState){}

			//When the state becomes inactive do this. 
			public virtual void OnExit(State nextState){}

			//Update things.
			public virtual void Update(){}

			//Clear resources
			public virtual void CleanUp(){}
		}
	}

}
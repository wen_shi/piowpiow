﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class GameScene : Scene<TransitionData> {

		private GameObject globals;
		private TransitionData.Mode _myMode;

		internal override void OnEnter(Scene<TransitionData> previousScene)
		{
			globals = GameObject.Find ("Globals");

			GameStart gs = GameObject.Find("Globals").GetComponent<GameStart>();
			gs.SwapSprite ();

			//get the mode before the globals 
			_myMode = previousScene.GetTransitionData ().myMode;  

			AddGlobalsComponents ();


		

		}

		private void AddGlobalsComponents()
		{
			//globals.AddComponent (Init);
			//Swap the background

			GameObject game = Resources.Load<GameObject> ("Game/Game");
			Instantiate (game);

			//Choose a mode depending on what the prev scene is. 
			if (_myMode == TransitionData.Mode.Classic) {
				Game.Instance ().myGameMode = Game.GameMode.Classic;
			}
			else {
				
				Game.Instance ().myGameMode = Game.GameMode.Random;
			}

		}

	}

}
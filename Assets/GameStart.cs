﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace gc{
	
	/// <summary>
	/// Game start- by loading in the Main menu scene/
	/// the one with the play button.
	/// </summary>
	public class GameStart : MonoBehaviour {

		//Make a scene manager reference.
		public static readonly SceneManager<TransitionData> SceneManager = new SceneManager<TransitionData> ();

		//Have an instance of this
		private static GameStart instance;
		public static GameStart Instance(){
			if (instance != null) {
				return instance;
			} else {
				GameObject gameStartObject = new GameObject();
				instance = gameStartObject.AddComponent<GameStart> ();
				return instance;
			}
		}

		//Keep track of the background
		public Sprite bg1;
		public Sprite bg2;

		private Text messageText;
		private SpriteRenderer sr;

		//Find the scene root on awaking to start the scenes.
		private void Awake()
		{
			SceneManager.SceneRoot = GameObject.Find ("SceneRoot");
			SceneManager.PushScene<TitleScene> ();

		}

		private void Start()
		{
			//Change the background on start. 
			sr = GameObject.Find("Background").GetComponent<SpriteRenderer>();
			sr.sprite = bg1;

			//MessageText
			messageText = GameObject.Find("Message").GetComponent<Text>();
		}

		public void SwapSprite()
		{
			if (sr.sprite == bg1) {
				sr.sprite = bg2;
			} else {
				sr.sprite = bg1;
			}
		}

		public void ChangeMessage(string message)
		{
			messageText.text = message;
		}
	}

	//Have some information for transitioning to another scene
	//not sure  how to yet 
	public struct TransitionData
	{
		public int playerScore;
		public string playerType;
		public enum Mode{Classic, Infinite}
		public Mode myMode;
	}


}
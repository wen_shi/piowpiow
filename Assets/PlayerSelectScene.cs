﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

namespace gc{

	public class PlayerSelectScene : Scene<TransitionData> {

		//The center of the screen.
		private Vector3 center = new Vector3 (60f, 40f, 0f);
		private float padding = 30f;

		//Prefab locations for the players
		private string BTN_PLAYER1 = "Prefabs/btn_player1";
		private string BTN_PLAYER2 = "Prefabs/btn_player2";
		private string BTN_PLAYER3 = "Prefabs/btn_player3";

		public enum PlayerType{piow, outlaw, bouncer}
		public static PlayerType myPlayerType;

		private TransitionData.Mode previousMode;

		//remove the button
		internal override void OnEnter(Scene<TransitionData> previousScene)
		{
			previousMode = previousScene.GetTransitionData().myMode;
		}

		// Use this for initialization
		void Start () {
		
			LoadButtons ();
		}

		/// <summary>
		/// Loading method to get button sfrom resources
		/// There is probably an easier way to do this. 
		/// Like make a method so you don't copy pasta. 
		/// Working hard is nice but working smart is better
		/// \(OwO)/
		/// </summary>
		private void LoadButtons()
		{
			//Make some game objects 
			GameObject buttonObject1, buttonObject2, buttonObject3;
			buttonObject1 = GetButtonObject (BTN_PLAYER1, -padding);
			buttonObject2 = GetButtonObject (BTN_PLAYER2, 0);
			buttonObject3 = GetButtonObject (BTN_PLAYER3, padding);
		}

		//Take a prefab and space it out. 
		private GameObject GetButtonObject(string prefab, float spacing)
		{
			UnityEngine.Object btn = ResourceLoader.Instance.GetResource(prefab);
			GameObject myObject = Instantiate (btn, new Vector3 (center.x + spacing, center.y, center.z), Quaternion.identity) as GameObject;

			myObject.transform.parent = gameObject.transform;
			return myObject;
		}

		internal override void OnExit (Scene<TransitionData> nextScene)
		{
			//print(myPlayerType); //starts out as piow . 
			//print ("going into the game as: " + myPlayerType);
		}

		internal override TransitionData GetTransitionData ()
		{
			return new TransitionData (){ myMode = previousMode };
		}
	}

}
﻿using UnityEngine;
using System.Collections;

namespace gc{
	
	public class ButtonClassic : Button {


		private ModeSelectScene parentScene;

		//assign sprites
		void Start () {

			normal = MODE_CLASSIC_NORMAL;
			down = MODE_CLASSIC_DOWN;
			over = MODE_CLASSIC_OVER;

			//assign the sr
			base.Start ();

			//Get the mode select scene
			parentScene = transform.parent.gameObject.GetComponent<ModeSelectScene>();
		}
		
		protected override void OnMouseDown()
		{
			base.OnMouseDown ();

			parentScene.selectMode = TransitionData.Mode.Classic;
			GameStart.SceneManager.Swap<PlayerSelectScene>();
		}
	}

}
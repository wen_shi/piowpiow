﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;


namespace gc{

/// <summary>
/// Button class, contains sprite information
	/// for its subclasses. Handles default mouse over,
	/// down, and normal sprite. Loads in the spritesheet. 
/// </summary>
public class Button : MonoBehaviour {

		#region spriteassignments
		//Caps because they won't be changed outside of this class.
		protected Sprite PLAY_BUTTON_NORMAL, PLAY_BUTTON_OVER, PLAY_BUTTON_DOWN;
		protected Sprite PLAYER_ONE_NORMAL, PLAYER_ONE_OVER, PLAYER_ONE_DOWN;
		protected Sprite PLAYER_TWO_NORMAL, PLAYER_TWO_OVER, PLAYER_TWO_DOWN;
		protected Sprite PLAYER_THREE_NORMAL, PLAYER_THREE_OVER, PLAYER_THREE_DOWN;
		protected Sprite MODE_CLASSIC_NORMAL, MODE_CLASSIC_OVER, MODE_CLASSIC_DOWN;
		protected Sprite MODE_INFINITE_NORMAL, MODE_INFINITE_OVER, MODE_INFINITE_DOWN; 

		private const string PLAY_BUTTONS_LOCATION = "Images/playButton";
		private const string PLAYER_SELECT_BUTTONS_LOCATION = "Images/playerselect";
		private const string MODE_BUTTONS_LOCATION = "Images/modeselect";

		protected Sprite normal, over, down; //The ones to use. 
		protected SpriteRenderer sr;

		#endregion

		/// <summary>
		/// Assign the locations of everything on awake. 
		/// </summary>
		void Awake(){

			//Locations
			Sprite[] playBtnSprites = Resources.LoadAll<Sprite> (PLAY_BUTTONS_LOCATION);
			Sprite[] playerSelectSprites = Resources.LoadAll<Sprite>(PLAYER_SELECT_BUTTONS_LOCATION);
			Sprite[] modeSelectSprites = Resources.LoadAll<Sprite> (MODE_BUTTONS_LOCATION);

			#region thisisamess
			//assign it to the variables. 
			PLAY_BUTTON_DOWN = playBtnSprites[0];
			PLAY_BUTTON_NORMAL = playBtnSprites [1];
			PLAY_BUTTON_OVER = playBtnSprites [2];

			PLAYER_ONE_DOWN = playerSelectSprites [0];
			PLAYER_ONE_NORMAL = playerSelectSprites [1];
			PLAYER_ONE_OVER = playerSelectSprites [2];
			PLAYER_TWO_DOWN = playerSelectSprites [3];
			PLAYER_TWO_NORMAL = playerSelectSprites [4];
			PLAYER_TWO_OVER = playerSelectSprites [5];
			PLAYER_THREE_DOWN = playerSelectSprites [6];
			PLAYER_THREE_NORMAL = playerSelectSprites[7];
			PLAYER_THREE_OVER = playerSelectSprites [8];

			//assign the modes, which happen to be in the right order. 
			MODE_CLASSIC_NORMAL = modeSelectSprites[0];
			MODE_CLASSIC_OVER = modeSelectSprites[1];
			MODE_CLASSIC_DOWN = modeSelectSprites[2];
			MODE_INFINITE_NORMAL = modeSelectSprites[3];
			MODE_INFINITE_OVER = modeSelectSprites[4];
			MODE_INFINITE_DOWN = modeSelectSprites[5];

			#endregion
		}

		//Assign the locations here. Override this. 
		protected virtual void Start(){

			sr = GetComponent<SpriteRenderer> ();
			Assert.IsNotNull(normal); //Make sure there's no null 
			sr.sprite = normal;
		}

		//Assign sprites to their normal/down/over states. 
		#region mouseMovement 

		protected virtual void OnMouseDown()
		{
			sr.sprite = down;
		}

		protected virtual void OnMouseOver()
		{
			sr.sprite = over;
		}

		protected virtual void OnMouseExit()
		{
			sr.sprite = normal;
		}

		#endregion

	}

}

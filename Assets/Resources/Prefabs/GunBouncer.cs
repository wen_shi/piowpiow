﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// A work in progress! 
	/// </summary>
	public class GunBouncer : Gun {

		private float myFireRate = 500f;

		protected override void Start ()
		{
			base.Start ();
			//also set the fire interval 
			FireInterval = myFireRate;
		}

		protected override void Fire ()
		{
			//Some other logic  here.
			//Consider different fire sounds for all. 

			BulletBouncer.Fire(_firingDirection, gameObject.transform.position, gameObject.name);
			_nextFireTime = Time.time + (FireInterval / 1000);
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_fire2");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, 0.1f);
		}



	}

}
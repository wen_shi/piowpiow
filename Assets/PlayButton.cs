﻿using UnityEngine;
using System.Collections;

namespace gc{

	public class PlayButton : Button {

		/// <summary>
		/// Assign the normal/down/over for this button
		/// Do the base/start (check sr)
		/// </summary>
		protected override void Start () {
			
			//Want to assign it before start,
			//Because the base checks to see if they're null.
			normal = PLAY_BUTTON_NORMAL;
			down = PLAY_BUTTON_DOWN;
			over = PLAY_BUTTON_OVER;

			base.Start ();
		}

		//Mouse down brings to the loaderthing. 
		protected override void OnMouseDown()
		{
			base.OnMouseDown ();
			//Move to the next loaderthing. 
			GameStart.SceneManager.Swap<ModeSelectScene>();

		}

		#region oldCodemightbestilluseful

		/*
		/// <summary>
		/// Take the prefab name and returns the associated
		/// sprite with it. 
		/// </summary>
		/// <returns>The sprite resource.</returns>
		/// <param name="prefabName">Prefab name.</param>
		Sprite GetSpriteResource(string prefabName)
		{
			//Cannot cast from source type to destination type.  why, unity. Why?
			//Sprite fab = (Sprite)ResourceLoader.Instance.GetResource (prefabName);
			Sprite fab =  Resources.Load<Sprite>(prefabName);
			return fab;
		}
		*/

		#endregion
	}

}

﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

namespace gc{

	/// <summary>
	/// Bullets for the bouncer player are 
	/// slower but have higher damage
	/// </summary>
	public class BulletBouncer : Bullet {

		#region attributes
		//From parent class 
		private float _myDamage = 100f;//100f;
		private float _mySpeed = 50f;

		//Detecting bouncing physics
		private bool isBouncing = false;

		private Rigidbody2D body;
		private bool zeroVelocity = false;

		//Raycast stuff
		private float radius = 20f;
		private float degrees = 90f;
		private bool drawme = false;


		#endregion

		//Override the deffault in start and set rb. 
		protected override void Start ()
		{
			Damage = _myDamage;
			Speed = _mySpeed;
			body = GetComponent<Rigidbody2D> ();
		}
			
		//Fire the same way
		//But on collision do omething different.
		protected override void OnCollisionEnter2D(Collision2D coll) 
		{
			//If it's an enemy, and you haven't collided before
			if (coll.gameObject.tag == "ENEMY" && !zeroVelocity) 
			{
				isBouncing = true;
				Enemy enemy = coll.gameObject.GetComponent<Enemy> ();
				Assert.IsNotNull (enemy, "Object tagged Enemy but no Enemy script attached");
				enemy.SendMessage ("ApplyDamage", Damage); 

				//feedback that it hit 

				//visual cue that its going to bounce 
				gameObject.transform.localScale *= 2;

				zeroVelocity = true;

				//play a clip

				AudioClip clip =  (AudioClip)ResourceLoader.Instance.GetResource("Sounds/bounce");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
				//print (coll.gameObject.name);
				DetectRaycast (coll.gameObject);

			} 
			else 
			{
				//print ("destroy me");
				Destroy (gameObject);

			}
		}

		/// <summary>
		/// Detects the raycast of what the object
		/// hits except itself (so yes it can bounce off the player)
		/// </summary>
		/// <param name="firstHitObject">First hit object.</param>
		private void DetectRaycast(GameObject firstHitObject)
		{
			//Detect raycast. 
			for (int i = -45; i < 45; i++) {

				//convert to radians because math
				float radians = i * Mathf.Deg2Rad;

				//This finds te end point at this angle of i
				//By getting the x/y of the degree and adding to radius
				//To find the end 
				float x = Mathf.Sin (radians);
				float y = Mathf.Cos (radians);

				Vector3 center = gameObject.transform.position;
				Vector3 endpoint = center + new Vector3 (radius * x, radius * y, 0f);

				//Get the direction of where to go 
				Vector3 direction = (endpoint - center).normalized;

				RaycastHit2D ray;
				Debug.DrawRay (center, direction, Color.red);
			//	print ("drawing ray");

				//Ok i am colliding with myself
				ray = Physics2D.Raycast (center, direction, radius);
				if (ray.collider != null) {

					//print ("first hit object:" + firstHitObject);
					//Make sure you're not hitting yourself
					if (ray.collider.gameObject != firstHitObject) 
					{
						
						MoveToObject (ray.collider.gameObject);
						SetDirection ();
						zeroVelocity = false;

						//If you found something, no need to keep castting for the 
						//Other degrees. Break out, be free. 
						break;
					}
				}
			}
		}

		//This is from enemyMovement

		private void MoveToObject(GameObject obj)
		{
			//Get direction to object
			Vector2 objectPosition = obj.transform.position;
			Vector2 myPos = gameObject.transform.position;

			Vector2 direction = (objectPosition - myPos).normalized;

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = direction * _mySpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, _mySpeed);
			body.AddForce(steeringForce);
		}

		/// <summary>
		/// Sets the direction so the 
		/// body faces the dir of the mvoement.
		/// </summary>
		private void SetDirection()
		{
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
		}

		protected virtual void Update()
		{
			//if the game is on pause return
			if (Game.Instance().IsPaused) { return; }
			//This is a workaround to setting the velocity in collisionenter because its not
			//working in there the objects still moving 
			if (zeroVelocity) {
				body.velocity = Vector2.zero;
				body.angularVelocity = 0f;
			}
		}
	}

}
﻿using UnityEngine;
using System.Collections;

namespace gc{

	/// <summary>
	/// Player outlaw starts out weaker than the standard player type. 
	/// With less lives and health. 
	/// </summary>
	public class PlayerOutlaw : Player {

		private int myStartLives = 2;
		private int myHealth = 50;
		private int myMaxHealth = 50;

		protected override void SetPlayerStats (int startLives, int health, int maxHealth)
		{
			//set my values to the params
			startLives = myStartLives;
			health = myHealth;
			maxHealth = myMaxHealth;

			//then set it to the player stats. 
			base.SetPlayerStats (startLives, health, maxHealth);
		}

}

}
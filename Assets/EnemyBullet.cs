﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

namespace gc{

	public class EnemyBullet : Bullet {

		//This is pretty bad tbh.
		//It's copy pasted from bullet but cant override the prefab
		//Because its static and gun needs this to be static. 
		internal static void FireEnemyBullet(Vector2 fireDirection, Vector3 position)
		{
			if (bulletPrototype == null) {
				bulletPrototype = ResourceLoader.Instance.GetResource("Prefabs/EnemyBullet");
			}

			GameObject bulletObject = Instantiate(bulletPrototype, position, Quaternion.identity) as GameObject;
			Bullet bullet = bulletObject.GetComponent<Bullet>();
			Rigidbody2D body = bulletObject.GetComponent<Rigidbody2D>();
			body.AddForce(fireDirection * bullet.Speed, ForceMode2D.Impulse);
		}

		protected override void OnCollisionEnter2D (Collision2D coll)
		{
			if (coll.gameObject.tag == "PLAYER") {
				Player player = coll.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "THERES NO PALYER SCRIPT ATTACHED");
				player.SendMessage ("ApplyDamage", Damage);

			}
		}

		void OnTriggerEnter2D(Collider2D col)
		{
			if (col.gameObject.tag == "PLAYER") {
				Debug.Log ("hit a player");
				Player player = col.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "THERES NO PALYER SCRIPT ATTACHED");
				player.SendMessage ("ApplyDamage", Damage);
				Destroy (this.gameObject);
			}
		}
		 

	}

}